<?php

/* Report all errors (Development) */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}

/* S4Portal Main Class */
class S4Portal
{

    /* Page variables */
    private $pageTitle = null;

    /* Notification variables */
    private $notify     = null;
    private $notifyType = null;
    private $notifyMsg  = null;

    /* Count variables for HighCharts on organisatie page */
    public $countBedrijf  = null;
    public $countSuspect  = null;
    public $countProspect = null;
    public $countInactief = null;

    /* S4Portal users variables */
    private $userIsActive     = null;  // (User is active on registration)
    private $userName         = null;
    private $userPass         = null;
    private $userEmail        = null;
    private $userRol          = null;
    private $userRolName      = null;
    private $userIsAdmin      = null;
    private $userIsRelManager = null;

    /* Organisation status (types) variables */
    private $statusId         = null;
    private $statusName       = null;

    /* Organisation variables */
    private $orgNumber         = null;
    private $orgId             = null;
    private $orgName           = null;
    private $orgAddress        = null;
    private $orgHouseNumber    = null;
    private $orgPostalCode     = null;
    private $orgCity           = null;
    private $orgCountry        = null;
    private $orgPhone          = null;
    private $orgEmail          = null;
    private $orgStatus         = null;
    private $orgNotes          = null;
    private $orgERP            = null;
    private $orgCMS            = null;
    private $orgIns            = null;
    private $orgInfDesk        = null;
    private $orgIncassoPartner = null;
    private $orgS4Prod         = null;
    private $orgS4ProdName     = null;
    private $orgRel            = null;
    private $orgRelId          = null;
    private $orgRelName        = null;
    private $orgCreator        = null;
    private $orgCreatedOn      = null;
    private $orgUpdatedOn      = null;
    private $orgUsername       = null;
    public  $orgExistence      = null;

    /* User variable */
    private $userID = null;

    /* Autocomplete search keyword variable */
    private $keyword = null;

    /* Contact details */
    private $conId         = null;
    private $conOrgId      = null;
    private $conOrgName    = null;
    private $conSex        = null;
    private $conFirstname  = null;
    private $conLastname   = null;
    private $conFunction   = null;
    private $conEmail      = null;
    private $conLandline   = null;
    private $conMobile     = null;
    private $conCreatedOn  = null;
    private $conUpdatedOn  = null;
    private $conType       = null;
    private $conStatusName = null;
    private $conMain       = null;
    private $conTypeLabel  = null;

    /* ERP variables */
    private $erpId        = null;
    private $erpName      = null;
    private $erpCreatedOn = null;
    private $erpUpdatedOn = null;

    /* Taken variables */
    private $taskFollowUp           = false;
    private $taskId                 = null;
    private $taskContent            = null;
    private $taskFollowUpDate       = null;
    private $taskFollowUpAction     = null;
    private $taskFollowUpLabel      = null;
    private $taskFollowUpImg        = null;
    private $taskFollowUpTitle      = null;
    private $taskFollowUpUser       = null;
    private $taskFollowUpActionId   = null;
    private $taskCreatorId          = null;
    
    private $taskCreatorName        = null;
    private $taskCreatedOn          = null;
    private $taskUpdatedOn          = null;
    private $taskRol                = null;
    private $taskOrgId              = null;
    private $taskOrgName            = null;

    /* Followup (opvolg) variables */
    private $followUpId                 = null;
    private $followUpActionId           = null;
    private $followUpTaskId             = null;
    private $followUpActionLabel        = null;
    private $followUpActionCreationDate = null;
    private $followUpActionUpdateDate   = null;
    private $followUpCreatorLabel       = null;

    /* S4Products variables */
    private $s4ProductId   = null;
    private $s4ProductName = null;
    private $s4CreatedOn   = null;
    private $s4UpdatedOn   = null;

    /* Kreditverzekeraars variables */
    private $kredietId        = null;
    private $kredietName      = null;
    private $kredietCreatedOn = null;
    private $kredietUpdatedOn = null;

    /* Informatiebureaus variables */
    private $infId        = null;
    private $infName      = null;
    private $infCreatedOn = null;
    private $infUpdatedOn = null;

    /* Relatiebeheerder variables */
    private $relId        = null;
    private $relName      = null;
    private $relCreatedOn = null;
    private $relUpdatedOn = null;

    /* Country & Language variables */
    private $countryId       = null;
    private $countryLang     = null;
    private $countryCode     = null;
    private $countryName     = null;
    private $countrySelected = null;

    /* File variables */
    private $fileId             = null;
    private $fileOrgId          = null;
    private $fileName           = null;
    private $fileType           = null;
    private $fileSize           = null;
    private $formattedFileSize  = null;
    private $fileCreatedOn      = null;
    private $fileUpdatedOn      = null;

    /* Getters */
    /* ORGANISATION */
    public function getOrgId()
    {
        return $this->orgId;
    }
    public function getOrgNumber()
    {
        return $this->orgNumber;
    }
    public function getOrgName()
    {
        return $this->orgName;
    }
    public function getOrgAddress()
    {
        return $this->orgAddress;
    }
    public function getOrgHouseNumber()
    {
        return $this->orgHouseNumber;
    }
    public function getOrgPostalcode()
    {
        return $this->orgPostalCode;
    }
    public function getOrgCity()
    {
        return $this->orgCity;
    }
    public function getOrgCountry()
    {
        return $this->orgCountry;
    }
    public function getOrgPhone()
    {
        return $this->orgPhone;
    }
    public function getOrgEmail()
    {
        return $this->orgEmail;
    }
    public function getOrgStatus()
    {
        return $this->orgStatus;
    }
    public function getOrgNotes()
    {
        return $this->orgNotes;
    }
    public function getOrgERP()
    {
        return $this->orgERP;
    }
    public function getOrgCMS()
    {
        return $this->orgCMS;
    }
    public function getOrgIns()
    {
        return $this->orgIns;
    }
    public function getOrgInfDesk()
    {
        return $this->orgInfDesk;
    }
    public function getOrgIncassopartner()
    {
        return $this->orgIncassoPartner;
    }
    public function getOrgS4Prod()
    {
        return $this->orgS4Prod;
    }
    public function getOrgS4ProdName()
    {
        return $this->orgS4ProdName;
    }
    public function getOrgRelId()
    {
        return $this->orgRel;
    }
    public function getOrgRelName()
    {
        return $this->orgUsername;
    }
    public function getOrgCreator()
    {
        return $this->orgCreator;
    }
    public function getOrgCreatedOn()
    {
        return $this->orgCreatedOn;
    }
    public function getOrgUpdatedOn()
    {
        /* Format date if not editted yet */
        if ($this->orgUpdatedOn === "0000-00-00 00:00:00") {
            $this->orgUpdatedOn = "Geen wijzigingen";
        }
        return $this->orgUpdatedOn;
    }
    /***********Contact getters************ */
    public function getConFirstname()
    {
        return $this->conFirstname;
    }
    public function getConLastname()
    {
        return $this->conLastname;
    }
    public function getConSex()
    {
        return $this->conSex;
    }
    public function getConId()
    {
        return $this->conId;
    }
    public function getConOrgId()
    {
        return $this->conOrgId;
    }
    public function getConFunction()
    {
        return $this->conFunction;
    }
    public function getConEmail()
    {
        return $this->conEmail;
    }
    public function getConLandline()
    {
        return $this->conLandline;
    }
    public function getConMobile()
    {
        return $this->conMobile;
    }
    public function getConType()
    {
        return $this->conType;
    }
    public function getConMain()
    {
        return $this->conMain;
    }
    public function getConCreatedOn()
    {
        return $this->conCreatedOn;
    }
    public function getConUpdatedOn()
    {
        return $this->conUpdatedOn;
    }



    /* Header Search function : Autocomplete for organisations */
    public function searchOrganisation($keyword)
    {

        $this->keyword = $keyword;
        include_once "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT id,organisatienaam,telefoon,email,status,s4_product FROM organisatie WHERE organisatienaam like '" . $this->keyword . "%' ORDER BY organisatienaam";
        $stmt = $conn->prepare($q);
        $stmt->execute();

        ?>
        <ul id="organisations-list">

            <?php
                    while ($row = $stmt->fetch()) {

                        $statusImg   = "";  // Org Type of customer Image
                        $statusTitle = "";
                        $productImg;
                        $titleImg     = $row['status'];      // Org Title
                        $titleProduct = $row['s4_product'];

                        if ($titleProduct == 1) {
                            $productImg = "assets/images/logo/S4D.png";
                            $titleProduct = "S4Dunning";
                        } else if ($titleProduct == 4) {
                            $productImg = "assets/images/logo/S4A.png";
                            $titleProduct = "S4Analysis";
                        } else if ($titleProduct == 2) {
                            $productImg = "assets/images/logo/S4P.png";
                            $titleProduct = "S4POP";
                        } else if ($titleProduct == 3) {
                            $productImg = "assets/images/logo/S4C.png";
                            $titleProduct = "S4Collections";
                        } else {
                            $productImg = "";
                        }

                        /* Prospect */
                        if ($row['status'] == "3") {
                            $statusImg = "assets/images/icon/prospect.png";
                            $statusTitle = "Prospect";
                        }
                        /* Suspect */ else if ($row['status'] === "2") {
                            $statusImg = "assets/images/icon/suspect.png";
                            $statusTitle = "Suspect";
                        }
                        /* Klant */ else if ($row['status'] === "4") {
                            $statusImg = "assets/images/icon/klant.png";
                            $statusTitle = "Klant";
                        }
                        /* Inactieve klant */ else if ($row['status'] === "1") {
                            $statusImg = "assets/images/icon/inactieve-klant.png";
                            $statusTitle = "Inactieve klant";
                            /* Niet herkend */
                        } else {
                            $statusImg = "";
                            $statusTitle = "(Soort klant niet herkend)";
                        }

                        ?>
                <a class="searchresult" href="organisatie.php?id=<?= $row['id'] ?>">
                    <img class="searchresult-img2" title="<?= $titleProduct ?>" width='26' src='<?= $productImg ?>'>&nbsp;<img class="searchresult-img" width='23' src='<?= $statusImg ?>'>
                    &nbsp;&nbsp;<?= $row["organisatienaam"]; ?></a>
            <?php }
                    ?>
        </ul>
        <?php }

            /* Count status:prospect of customers for HIGHCHARTS */
            public function getCountProspect()
            {

                include_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT COUNT(id) as countProspect FROM organisatie WHERE status='Prospect'";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    return $row["countProspect"];
                }
            }

            /* Count status:Bedrijf of customers for HIGHCHARTS */
            // public function getCountKlant()
            // {

            //     include_once "dbconfig.php";
            //     $conn = DatabaseConnection::getConnection();
            //     $q = "SELECT COUNT(id) as countBedrijf FROM organisatie WHERE status='Klant'";
            //     $stmt = $conn->prepare($q);
            //     $stmt->execute();

            //     while ($row = $stmt->fetch()) {
            //         return $row["countBedrijf"];
            //     }
            // }

            /* Count status:Suspect of customers for HIGHCHARTS */
            public function getCountSuspect()
            {

                include_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT COUNT(id) as countSuspect FROM organisatie WHERE status='Suspect'";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    return $row['countSuspect'];
                }
            }

            /* Count status:Inactief of customers for HIGHCHARTS */
            public function getCountInactief()
            {

                include_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT COUNT(id) as countInactief FROM organisatie WHERE status='Inactieve klant'";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    return $row["countInactief"];
                }
            }

            /* Get all ERP Systems options */
            public function getAllERPOptions()
            {
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT * FROM erp";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    $this->erpId = $row['id'];
                    $this->erpName = $row['name'];
                    $this->erpCreatedOn = $row['created_on'];
                    $this->erpUpdatedOn = $row['updated_on'];
                    ?>
            <option value="<?= $this->erpName ?>"></option>
        <?php }
            }

            /* Get all S4 Products options */
            public function getAllS4ProductsOptions()
            {
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT * FROM s4producten";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    $this->s4ProductId = $row['id'];
                    $this->s4ProductName = $row['name'];
                    $this->s4CreatedOn = $row['created_on'];
                    $this->s4UpdatedOn = $row['updated_on'];
                    ?>
            <option value="<?= $this->s4ProductId ?>"><?= $this->s4ProductName ?></option>
        <?php }
            }


            /* Get all kredietverzekeraars options */
            public function getAllKredietVerzekeraarsOptions()
            {
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT * FROM kredietverzekeraars";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    $this->kredietId = $row['id'];
                    $this->kredietName = $row['name'];
                    $this->kredietCreatedOn = $row['created_on'];
                    $this->kredietUpdatedOn = $row['updated_on'];
                    ?>
            <option value="<?= $this->kredietName ?>"></option>
        <?php }
            }


            /* Get all informatiebureau options */
            public function getAllInformatiebureauOptions()
            {
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT * FROM informatiebureaus";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    $this->infId = $row['id'];
                    $this->infName = $row['name'];
                    $this->infCreatedOn = $row['created_on'];
                    $this->infUpdatedOn = $row['updated_on'];
                    ?>
            <option value="<?= $this->infName ?>"></option>
        <?php }
            }

            /* Get all users(login s4portal) */
            public function getAllS4PortalUsers()
            {

                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT * FROM gebruikers";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {

                    $this->userID = $row['id'];
                    $this->userName = $row['id'];
                    $this->userEmail = $row['id'];
                    $this->userPass = $row['id'];
                    $this->user = $row['id'];
                }
            }


            /* Get all relatiebeheerder options */
            public function getAllRelatiebeheerderOptions()
            {
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT * FROM gebruikers WHERE isRelatiebeheerder = 1";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    $this->relId        = $row['id'];
                    $this->relName      = $row['gebruikersnaam'];
                    $this->relCreatedOn = $row['created_on'];
                    $this->relUpdatedOn = $row['updated_on'];
                    ?>
            <option class="reloption" data-optionid="<?= $this->relId ?>" data-optionname="<?= $this->relName ?>" value="<?= $this->relId ?>"><?= $this->relName ?></option>
            <?php }
                }

                /* Get all users to whom you can assign tasks */
                public function getAllTaskUsers()
                {
                    require_once "dbconfig.php";
                    $conn = DatabaseConnection::getConnection();
                    $q = "SELECT * FROM gebruikers WHERE bot=0";
                    $stmt = $conn->prepare($q);
                    if ($stmt->execute()) {
                        while ($row = $stmt->fetch()) {
                            $this->userID   = $row['id']; // User ID
                            $this->userName = $row['gebruikersnaam']; // User Name
                            ?>
                <option value="<?= $this->userID ?>"><?= $this->userName ?></option>
            <?php }
                    }
                }

                /* Get all task actions */
                public function getAllTaskActions()
                {
                    $conn = DatabaseConnection::getConnection();
                    $q = "SELECT * FROM opvolgacties";
                    $stmt = $conn->prepare($q);
                    if ($stmt->execute()) {
                        while ($row = $stmt->fetch()) {
                            $this->followUpActionId           = $row['id'];          // User ID
                            $this->followUpActionLabel        = $row['name'];        // Action label
                            $this->followUpActionCreationDate = $row['created_on'];  // Creation date
                            $this->followUpActionUpdateDate   = $row['updated_on'];  // Update date
                            ?>
                <option value="<?= $this->followUpActionId ?>"><?= $this->followUpActionLabel ?></option>
            <?php }
                    }
                }


                /* Get all Status options */
                public function getAllStatusOptions()
                {
                    require_once "dbconfig.php";
                    $conn = DatabaseConnection::getConnection();
                    $q = "SELECT * FROM klantsoorten ORDER BY id";
                    $stmt = $conn->prepare($q);
                    if ($stmt->execute()) {
                        while ($row = $stmt->fetch()) {
                            $this->statusId   = $row['id'];
                            $this->statusName = $row['name'];
                            ?>
                <option value="<?= $this->statusId ?>"><?= $this->statusName ?></option>
            <?php }
                    }
                }

                /* Get all user roles options */
                public function getAllRoleOptions()
                {

                    require_once "dbconfig.php";
                    $conn = DatabaseConnection::getConnection();
                    $q = "SELECT * FROM rollen";
                    $stmt = $conn->prepare($q);
                    if ($stmt->execute()) {
                        while ($row = $stmt->fetch()) {
                            $this->userRol     = $row['id'];
                            $this->userRolName = $row['rol'];
                            ?>
                <option value="<?= $this->userRol ?>"><?= $this->userRolName ?></option>
            <?php }
                    } else {
                        echo "Ophalen van rollen niet gelukt / Query niet uitgevoerd...";
                    }
                }

                /* Get all bijlagen */
                public function getAllBijlagen()
                {
                    require_once "dbconfig.php";
                    $conn = DatabaseConnection::getConnection();
                    $q = "SELECT organisatie.organisatienaam, bijlagen.id AS fileId, bijlagen.orgId, bijlagen.name, bijlagen.type, bijlagen.size, bijlagen.created_on, bijlagen.updated_on, organisatie.id
        FROM bijlagen
        INNER JOIN organisatie ON bijlagen.orgId=organisatie.id;";
                    $stmt = $conn->prepare($q);
                    $stmt->execute();

                    while ($row = $stmt->fetch()) {

                        $this->fileId        = $row['fileId'];
                        $this->fileOrgId     = $row['orgId'];
                        $this->orgName       = $row['organisatienaam'];
                        $this->fileName      = $row['name'];
                        $this->fileType      = $row['type'];
                        $this->fileSize      = $row['size'];
                        $this->fileCreatedOn = $row['created_on'];
                        $this->fileUpdatedOn = $row['updated_on'];

                        /* Format dates */
                        $createDate = date_create($this->fileCreatedOn);
                        $this->fileCreatedOn = date_format($createDate, 'd-m-Y');
                        $this->fileSize = round($this->fileSize / 1000000, 1);

                        /* Format file name */
                        if ($this->fileType === "application/pdf") {
                            $this->fileType = "<img title='PDF' width='30' src='assets/images/extensions/pdf.png'><span style='display:none;'>PDF</span>";
                        }
                        if ($this->fileType === "application/pdf") {
                            $this->fileType = "<img title='PDF' width='30' src='assets/images/extensions/pdf.png'><span style='display:none;'>PDF</span>";
                        }

                        ?>
            <tr>
                <td scope="col"><?= $this->orgName; ?></td>
                <td class="filename" data-filename="<?= $this->fileName ?>" scope="col"><a href="uploads/<?= $this->fileName ?>"><?= $this->fileName; ?></a></td>
                <td scope="col"><?= $this->fileSize ?></td>
                <!-- <td scope="col">filetype</td> -->
                <td scope="col"><?= $this->fileCreatedOn ?></td>
                <td>
                    <ul class="d-flex justify-content-center">
                        <li><a title="Bestand verwijderen" href="controller.php?del_id=<?= $this->fileId ?>&del_name=<?= $this->fileName ?>" class="text-danger"><i class="ti-trash del-file"></i></a></li>
                    </ul>
                </td>
            </tr>
        <?php }
            }

            /* Render all S4Portal users */
            public function renderUserCreationTab()
            { ?>

        <!-- Gebruiker aanmaken tab -->
        <div class="card">
            <div class="card-header">
                <a style="color:#08475b" class="card-link collapsed" data-toggle="collapse" href="#accordion21" aria-expanded="false"><i class="ti-add"></i>&nbsp;&nbsp;Gebruikers aanmaken</a>
            </div>

            <div id="accordion21" class="collapse" data-parent="#accordion2" style="">
                <div class="card-body">
                    <div class="row">

                        <div class="col-lg-6 mt-3">

                            <!-- Start form -->
                            <form method="POST" action="controller.php?newUser" autocomplete="off">

                                <!-- Gebruikersnaam -->
                                <div class="form-group col-lg-12 mt-3">
                                    <label for="username" class="col-form-label noselect">* Naam</label>
                                    <input name="username" class="form-control" type="text" value="" id="username" required>
                                </div>

                                <!-- Emailadres -->
                                <div class="form-group col-lg-12 mt-3">
                                    <label for="email" class="col-form-label noselect">* Emailadres</label>
                                    <input name="email" class="form-control" type="email" value="" id="email" required>
                                </div>

                                <!-- Wachtwoord -->
                                <div class="form-group col-lg-12 mt-3">
                                    <label for="password" class="col-form-label noselect">* Wachtwoord</label>
                                    <input name="password" class="form-control" type="password" value="" id="password" required>
                                </div>

                        </div>

                        <!-- right side of form -->
                        <div class="col-lg-6 mt-3">

                            <div class="row">

                                <!-- Rol -->
                                <div class="form-group col-lg-12 mt-3">
                                    <label for="rol" class="col-form-label noselect">* Rol</label>
                                    <select class="form-control" id="rol" name="rol" required>
                                        <?= $this->getAllRoleOptions() ?>
                                    </select>
                                </div>

                                <!-- IsAdmin -->
                                <div class="form-group col-lg-6 mt-3">
                                    <label for="isAdmin" class="col-form-label noselect">* S4Portal Administrator</label>
                                    <select class="form-control" id="isAdmin" name="isAdmin" required>
                                        <option value="1">Ja</option>
                                        <option selected value="0">Nee</option>
                                    </select>
                                </div>

                                <!-- isRelatieBeheerder -->
                                <div class="form-group col-lg-6 mt-3">
                                    <label for="isRelManager" class="col-form-label noselect">* Relatiebeheerder</label>
                                    <select class="form-control" id="isRelManager" name="isRelManager" required>
                                        <option value="1">Ja</option>
                                        <option selected value="0">Nee</option>
                                    </select>
                                </div>

                            </div>

                            <!-- Type of contact: Sales, Support or administration -->
                            <input name="conType" type="hidden" value="sales">

                            <!-- Submit button -->
                            <button class="btn btn-primary btn-lg btn-block" type="submit" name="submitNewContact"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Gebruiker aanmaken</button>

                        </div>
                        </form>
                        <!-- End form -->
                    </div>
                </div>
            </div>
        </div>

    <?php }


        /* Render organisation creation table */
        public function renderOrganisationCreationView()
        { ?>
        <!-- Form title -->
        <h4 class="header-title noselect" style="color:#08475b"><i class="ti-organisations"></i>&nbsp;Organisatie aanmaken 1/2</h4>

        <!-- Start form -->
        <form method="POST" action="controller.php?newOrganisation" autocomplete="off">

            <div class="row">

                <!-- Organisatienaam -->
                <div class="form-group col-md-6">
                    <label for="orgName" class="col-form-label noselect">* Organisatienaam</label>
                    <input name="orgName" class="form-control" type="text" value="" id="orgName" required>
                </div>

                <!-- Organisatienummer -->
                <div class="form-group col-md-6">
                    <label for="orgNumber" class="col-form-label noselect">Organisatienummer</label>
                    <input name="orgNumber" class="form-control" type="text" value="" id="orgNumber">
                </div>

            </div>

            <div class="row">

                <!-- Adres -->
                <div class="form-group col-md-4">
                    <label for="orgAddress" class="col-form-label noselect">* Adres</label>
                    <input name="orgAddress" class="form-control" type="text" value="" id="orgAddress" required>
                </div>

                <!-- Huisnummer -->
                <div class="form-group col-md-2">
                    <label for="orgHouseNumber" class="col-form-label noselect">* Huisnr.</label>
                    <input name="orgHouseNumber" class="form-control" type="text" value="" id="orgHouseNumber" required>
                </div>

                <!-- Postcode -->
                <div class="form-group col-md-2">
                    <label for="orgPostalCode" class="col-form-label noselect">* Postcode</label>
                    <input name="orgPostalCode" class="form-control" type="text" value="" id="orgPostalCode" required>
                </div>

                <!-- Plaatsnaam (stad) -->
                <div class="form-group col-md-4">
                    <label for="orgCity" class="col-form-label noselect">* Plaatsnaam</label>
                    <input name="orgCity" class="form-control" type="text" value="" id="orgCity" required>
                </div>
            </div>

            <!-- Plaatsnaam (land) -->
            <div class="form-group">
                <label for="orgCountry" class="col-form-label noselect">* Land</label>
                <select name="orgCountry" class="form-control">
                    <?= $this->getAllCountryOptions() ?>
                </select>
            </div>

            <!-- Telefoonnummer -->
            <div class="form-group">
                <label for="orgPhone" class="col-form-label noselect">* Telefoonnummer</label>
                <input name="orgPhone" type="text" class="form-control" id="orgPhone" value="" required>
            </div>

            <!-- Emailadres -->
            <div class="form-group">
                <label for="orgEmail" class="col-form-label noselect">* Emailadres</label>
                <input name="orgEmail" class="form-control" type="email" value="" id="orgEmail" required>
            </div>

            <!-- Klant status -->
            <div class="form-group">
                <label for="orgStatus" class="col-form-label noselect">* Status</label>
                <select name="orgStatus" class="form-control" required>
                    <?= $this->getAllStatusOptions() ?>
                </select>
            </div>

            <div class="row">

                <!-- Incassopartner -->
                <div class="form-group col-md-6">
                    <label for="orgIncassoPartner" class="col-form-label noselect">Incassopartner</label>
                    <input name="orgIncassoPartner" class="form-control" type="text" value="" id="orgIncassoPartner">
                </div>

                <!-- Relatiebeheerder -->
                <div class="form-group col-md-6">
                    <label for="orgRel" class="col-form-label noselect">* Relatiebeheerder</label>
                    <select id="orgRel" name="orgRel" class="form-control" required>
                        <?= $this->getAllRelatiebeheerderOptions() ?>
                    </select>
                </div>

            </div>
            </div>
            </div>
            </div>

            <!-- Right side of form -->
            <div class="col-6 mt-3">
                <div class="card">
                    <div class="card-body">
                        <!-- Form title -->
                        <h4 class="header-title noselect" style="color:#08475b"><i class="ti-organisations"></i>&nbsp;Organisatie aanmaken 2/2</h4>

                        <!-- S4 Product -->
                        <div class="form-group">

                            <label for="orgS4Prod" class="col-form-label noselect">S4 Product</label>
                            <!-- <input list="orgS4Prod" name="orgS4Prod" class="S4ProdDatalist" placeholder="Klik hier om de lijst op te halen of voer zelf wat in..." required> -->
                            <select class="S4ProdDatalist" name="orgS4Prod" class="form-control hidden" id="orgS4Prod">
                                <?php echo $this->getAllS4ProductsOptions(); ?>
                            </select>

                        </div>

                        <!-- CMS Systemen -->
                        <div class="form-group">
                            <label for="orgCMS" class="col-form-label noselect">CMS Systeem</label>
                            <input list="orgCMS" name="orgCMS" class="S4ProdDatalist" placeholder="Klik hier om de lijst op te halen of voer zelf wat in...">
                            <datalist name="orgCMS" class="form-control hidden" id="orgCMS">
                                <option selected value="CreditDevice"></option>
                                <option value="Debisoft"></option>
                                <option value="iController"></option>
                                <option value="Maxcredible"></option>
                                <option value="OnGuard"></option>
                                <option value="Payt"></option>
                                <option value="Sidetrade"></option>
                                <option value="TKB"></option>
                            </datalist>
                        </div>


                        <!-- ERP Systemen -->
                        <div class="form-group">
                            <label for="orgERP" class="col-form-label noselect">ERP Systeem</label>
                            <input list="orgERP" name="orgERP" class="S4ProdDatalist" placeholder="Klik hier om de lijst op te halen of voer zelf wat in...">
                            <datalist name="orgERP" class="form-control hidden" id="orgERP">
                                <?= $this->getAllERPOptions(); ?>
                            </datalist>
                        </div>

                        <!-- Kreditverzekeraar -->
                        <div class="form-group">
                            <label for="orgINS" class="col-form-label noselect">Kredietverzekeraar</label>
                            <input list="orgINS" name="orgINS" class="S4ProdDatalist" placeholder="Klik hier om de lijst op te halen of voer zelf wat in...">
                            <datalist name="orgINS" class="form-control hidden" id="orgINS">
                                <?= $this->getAllKredietVerzekeraarsOptions(); ?>
                            </datalist>
                        </div>


                        <!-- Informatiebureau -->
                        <div class="form-group">
                            <label for="orgInfDesk" class="col-form-label noselect">Informatiebureau</label>
                            <input list="orgInfDesk" name="orgInfDesk" class="S4ProdDatalist" placeholder="Klik hier om de lijst op te halen of voer zelf wat in...">
                            <datalist name="orgInfDesk" class="form-control hidden" id="orgInfDesk">
                                <?= $this->getAllInformatiebureauOptions(); ?>
                            </datalist>
                        </div>

                        <!-- Notities -->
                        <div class="form-group">
                            <label for="orgNotes" class="col-form-label noselect">Notities</label>
                            <input name="orgNotes" class="form-control" type="text" value="" id="orgNotes">
                        </div>
                        <span style="display:inline-block;height:24px"></span>
                    </div>
                </div>

                <!-- Submit button -->
                <button class="btn btn-primary btn-lg btn-block" type="submit" name="submitNewOrganisation"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Organisatie opslaan</button>
            </div>
        </form>
        <!-- End form -->

        <?php }


            /* Delete tasks */
            public function deleteTask()
            { }

            /* Get all tasks */
            public function renderTakenTable()
            {
                $this->userId = $_SESSION['user_id'];
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT
                taken.id AS `task_id`,                                    
                taken.taak AS `task_content`,
                                taken.creator AS `task_creator_id`,
                taken.created_on AS `task_creation_date`,
                taken.updated_on AS `task_updated_date`,
                taken.organisatie AS `task_org_id`,
                taskOrg.organisatienaam AS `task_org_name`, 
                creator.gebruikersnaam AS `task_creator_name`,
                contacts.voornaam AS `contact_firstname`,
                contacts.achternaam AS `contact_lastname`,
                contacts.telefoon_vast AS `contact_landline`
                FROM taken
                INNER JOIN gebruikers AS `creator` ON creator.id = taken.creator
                INNER JOIN organisatie AS `taskOrg` ON taken.organisatie = taskOrg.id
                INNER JOIN contactpersonen AS `contacts` on contacts.orgId = taskOrg.id
                
                WHERE contacts.main=1 AND taken.heeft_opvolging = 1
                ";
                $stmt = $conn->prepare($q);

                /* If query succesfully executed */
                if ($stmt->execute()) {
                    while ($row = $stmt->fetch()) {

                        /* Assign class variables from database */
                        $this->taskId               = $row['task_id'];                                       // Task ID            
                        $this->taskContent          = $row['task_content'];                                  // Task content
                        $this->taskFollowUpDate     = $row['task_followup_date'];                            // Task Follow up date
                        $this->taskFollowUpActionId = $row['task_followup_id'];                              // Task Follow-up action ( ID of action )
                        $this->taskFollowUpLabel    = $row['task_followup_label'];                           // Task Follow-up label / name
                        $this->taskFollowUpUser     = $row['task_actionholder_name'];                        // Task Follow-up user
                        $this->taskOrgId            = $row['task_org_id'];                                   // Task Organisation ID
                        $this->taskOrgName          = $row['task_org_name'];                                 // Task Organisation Name
                        $this->taskCreatorId        = $row['task_creator_id'];                               // Task creator ( ID )
                        $this->taskCreatorName      = $row['task_creator_name'];                             // Task creator ( Name )
                        $this->taskCreatedOn        = date('d-m-Y', strtotime($row['task_creation_date']));  // Task creation date
                        $this->taskUpdatedOn        = $row['task_updated_date'];                             // Task update date
                        $this->taskContact          = $row['contact_firstname'] . "&nbsp;" . $row['contact_lastname'];
                        $this->conLandline          = $row['contact_landline'];

                        /* If Opvolgen false */
                        if ($this->taskFollowUpActionId === NULL) {
                            $this->taskFollowUpTitle = "Nee";
                            //$this->taskFollowUpImg = "<img title='".$this->taskFollowUpTitle."' width='15' src='assets/images/icon/no.png'>";
                        }
                        /* If Opvolgen true */ else {
                            $this->taskFollowUpTitle = "Ja";
                            //$this->taskFollowUpImg = "<img title='".$this->taskFollowUpTitle."' width='15' src='assets/images/icon/yes.png'>";
                        }

                        /* Format opvolgdatum */
                        if ($this->taskFollowUpDate === NULL) {
                            $this->taskFollowUpDate = "";
                        } else {
                            $this->taskFollowUpDate = date('d-m-Y', strtotime($row['task_followup_date']));
                        }
                        ?>

                <tr data-row-id="<?= $this->taskId ?>">
                    <!-- Task Organisation -->
                    <td><a href="organisatie.php?id=<?= $this->taskOrgId ?>"><?= $this->taskOrgName ?></a></td>

                    <!-- Task Title -->
                    <td><?= $this->taskContent ?></td>
                    <!-- Task creator -->
                    <td><?= $this->taskCreatorName ?></td>
                    <!-- Task Follow-Up User / action holder -->
                    <td><?= $this->taskFollowUpUser ?></td>
                    <!-- Task Follow-Up Label/name -->
                    <td><?= $this->taskFollowUpLabel ?></td>
                    <!-- Follow-Up Date -->
                    <td><?= $this->taskFollowUpDate ?></td>
                    <!-- Task Contactpersoon -->
                    <td><?= $this->taskContact ?></td>
                    <!-- Task Contactpersoon -->
                    <td><?= $this->conLandline ?></td>

                    <!-- Enable / disable following up a task(action) based on taskFollowUp nullable -->
                    <?php
                                    if ($this->taskFollowUpActionId !== NULL) { ?>
                        <td>
                            <ul class="d-flex justify-content-center">
                                <li data-task-id="<?= $this->taskId ?>" data-task-org-id="<?= $this->taskOrgId ?>" data-task="<?= $this->taskContent ?>" data-task-creator="<?= $this->taskCreatorName ?>" data-task-creationdate="<?= $this->taskCreatedOn ?>" data-task-followup-label="<?= $this->taskFollowUpLabel ?>" data-task-followup-owner="<?= $this->taskFollowUpUser ?>" data-task-followup-date="<?= $this->taskFollowUpDate ?>" class="assign-task mr-3"><i data-toggle="modal" data-target="#tasksModal" data-id="" title="Taak op laten volgen" class="fa fa-bolt assign-task"></i></li>
                            </ul>
                        </td>
                    <?php } else { ?>
                        <td>
                            <ul class="d-flex justify-content-center">
                            </ul>
                        </td>
                    <?php }
                                    ?>
                </tr>
        <?php }
                } else {
                    echo "Running SQL query failed or no Database Connection (File: s4portal.class, method: renderTakenTable())";
                }
            }

            /* Render the active follow-up table */
            public function renderFollowUpTakenTable()
            {
                $this->userRol = $_SESSION['userRol'];
                $this->orgId = $_GET['id'];

                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "select * from (
                    select *,
                    (select opvolgacties.name from opvolgingen left join opvolgacties on opvolgingen.opvolgactie = opvolgacties.id where opvolgingen.taak_id=taken.id and  opvolgingen.rol = 2
                                    AND opvolgingen.afgehandeld=0 order by opvolgingen.opvolgdatum limit 1) as opvolgactie,
                        
                    (select opvolgingen.opvolgdatum from opvolgingen where opvolgingen.taak_id=taken.id and  opvolgingen.rol = 2
                                    AND opvolgingen.afgehandeld=0 order by opvolgingen.opvolgdatum limit 1) as opvolgdatum,
                        
                    (select gebruikers.gebruikersnaam from opvolgingen left join gebruikers on opvolgingen.actiehouder = gebruikers.id where opvolgingen.taak_id=taken.id and  opvolgingen.rol = 2
                                    AND opvolgingen.afgehandeld=0 order by opvolgingen.opvolgdatum limit 1) as actiehouder_naam,
                    
                    (select concat(contactpersonen.voornaam,' ',contactpersonen.achternaam) from opvolgingen left join contactpersonen on opvolgingen.organisatie = contactpersonen.orgId and contactpersonen.main = 1 where opvolgingen.taak_id=taken.id and  opvolgingen.rol = 2
                                    AND opvolgingen.afgehandeld=0 order by opvolgingen.opvolgdatum limit 1 ) as contactpersoon_naam,
                        
                    (select contactpersonen.telefoon_vast from opvolgingen left join contactpersonen on opvolgingen.organisatie = contactpersonen.orgId and contactpersonen.main = 1 where opvolgingen.taak_id=taken.id and  opvolgingen.rol = 2
                                    AND opvolgingen.afgehandeld=0 order by opvolgingen.opvolgdatum limit 1 ) as contactpersoon_tel_vast,
                    
                    (select contactpersonen.telefoon_mobiel from opvolgingen left join contactpersonen on opvolgingen.organisatie = contactpersonen.orgId and contactpersonen.main = 1 where opvolgingen.taak_id=taken.id and  opvolgingen.rol = 2
                                    AND opvolgingen.afgehandeld=0 order by opvolgingen.opvolgdatum limit 1 ) as contactpersoon_tel_mobiel
                
                    from taken
                    where taken.organisatie=127) t100
                    order by opvolgdatum";
                $stmt = $conn->prepare($q);

                if ($stmt->execute()) {

                    while ($row = $stmt->fetch()) {

                        $this->followUpTaskLabel          = $row['taak'];
                        $this->followUpActionLabel        = $row['opvolgactie'];
                        $this->followUpActionCreationDate = $row['opvolgdatum'];
                        $this->followUpActionHolder       = $row['actiehouder_naam'];
                        $this->followUpContact            = $row['contactpersoon_naam'];
                        $this->followUpContactLandline    = $row['contactpersoon_tel_vast'];
                        $this->followUpContactMobile      = $row['contactpersoon_tel_mobiel'];

                        // $this->followUpId                = $row['opvolging_id'];
                        // $this->followUpTaskId            = $row['opvolging_taak_id'];
                        // $this->followUpTaskLabel         = $row['opvolging_taak_label'];
                        // $this->followUpOrgId             = $row['opvolging_organisatie_id'];
                        // $this->followUpOrgLabel          = $row['opvolging_organisatie_label'];
                        // $this->followUpRolId             = $row['opvolging_rol_id'];
                        // $this->followUpRolLabel          = $row['opvolging_rol_label'];
                        // $this->followUpActionId          = $row['opvolging_opvolgactie_id'];
                        // $this->followUpActionLabel       = $row['opvolging_opvolgactie_label'];
                        // $this->followUpDate              = $row['opvolging_opvolgdatum'];
                        // $this->followUpActionHolderId    = $row['opvolging_actiehouder_id'];
                        // $this->followUpActionHolderLabel = $row['opvolging_actiehouder_label'];
                        // $this->followUpCreatorId         = $row['opvolging_creator_id'];
                        // $this->followUpCreatorLabel      = $row['opvolging_creator_id'];
                        // $this->followUpCreatedOn         = $row['opvolging_created_on'];
                        // $this->followUpUpdatedOn         = $row['opvolging_updated_on'];
                        // //$this->originalTaskLabel         = $row['original_taak_label'];
                        // $this->conFirstname              = $row['contact_firstname'];
                        // $this->conLastname               = $row['contact_lastname'];
                        // $this->conLandline               = $row['contact_landline'];
                        // $this->conMobile                 = $row['contact_mobile'];
                        ?>
                        <tr data-org="<?=$this->orgId?>">

                            <td id=""><a href="#" data-toggle="modal" data-target="#historyModal" data-history-id="<?=$this->followUpTaskId?>" title="Eerdere opvolgingen inzien" class="task-history"><?= $this->followUpTaskLabel ?></a></td>
                            <td id=""><?= $this->followUpActionHolder ?></td>
                            <td id=""><?= $this->followUpActionLabel ?></td>
                            <td id=""><?= date("d-m-Y", strtotime($this->followUpActionCreationDate)) ?></td>
                            <td id=""><?=$this->followUpContact?></td>
                            <td id=""><?="0306031040"?></td>
                            <td>
                                <ul class="d-flex justify-content-center">
                                    <li data-original-task-id="<?=$this->followUpTaskId?>" data-task-id="<?= $this->followUpId ?>" data-task-org-id="<?= $this->followUpOrgId ?>" data-task="<?= $this->followUpTaskLabel ?>" data-task-creator="<?= $this->followUpCreatorLabel ?>" data-task-creationdate="<?= $this->followUpCreatedOn ?>" data-task-followup-label="<?= $this->followUpTaskLabel ?>" data-task-followup-owner="<?= $this->followUpActionHolderId ?>" data-task-followup-date="<?= $this->followUpDate ?>" class="assign-task mr-3"><i data-toggle="modal" data-target="#tasksModal" data-id="" title="Taak op laten volgen" class="fa fa-bolt assign-task"></i></li>
                                    <li class="complete-task mr-3"><i onclick="confirm('weet je het zeker?')" title="Taak afhandelen" class="fa fa-check complete-task"></i></li>
                                </ul>
                        </td>
                        </tr>
                        <?php
                       
                    }
                } else {
                    echo "Query not executed";
                }
            }


            /* Render the done follow-up table */
            public function renderSettledFollowUpTakenTable()
            { 
                echo "hoi";
                exit();

                $this->userId = $_SESSION['user_id'];
                $this->orgId = $_GET['id'];
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT 
                opvolgingen.id AS `opvolging_id`,                                    
                opvolgingen.taak_id AS `opvolging_taak_id`,
                opvolgingen.taak AS `opvolging_taak_label`,
                opvolgingen.organisatie AS `opvolging_organisatie_id`,
                organisatie.organisatienaam AS `opvolging_organisatie_label`,
                opvolgingen.rol AS `opvolging_rol_id`,
                (select rol from rollen where id=opvolgingen.rol) as `opvolging_rol_label`,
                opvolgingen.opvolgactie AS `opvolging_opvolgactie_id`,
                opvolgacties.name AS `opvolging_opvolgactie_label`,
                opvolgingen.opvolgdatum AS `opvolging_opvolgdatum`,
                opvolgingen.actiehouder AS `opvolging_actiehouder_id`,
                gebruikers.gebruikersnaam AS `opvolging_actiehouder_label`,
                opvolgingen.creator AS `opvolging_creator_id`, 
                opvolgingen.created_on AS `opvolging_created_on`, 
                opvolgingen.updated_on AS `opvolging_updated_on`,
                taken.taak AS `original_taak_label`
                FROM taken
				INNER JOIN gebruikers ON opvolgingen.actiehouder = gebruikers.id
                INNER JOIN organisatie ON opvolgingen.organisatie = organisatie.id
                INNER JOIN opvolgacties ON opvolgingen.opvolgactie = opvolgacties.id
                INNER JOIN taken ON opvolgingen.taak_id = taken.id
                WHERE opvolgingen.actiehouder = '$this->userId' AND opvolgingen.afgehandeld=1 AND organisatie.id = '$this->orgId'
                ORDER BY opvolgingen.opvolgdatum LIMIT 1";
               
                $stmt = $conn->prepare($q);
                if ($stmt->execute()) {
                    while ($row = $stmt->fetch()) {
                        $this->followUpId                = $row['opvolging_id'];
                        $this->followUpTaskId            = $row['opvolging_taak_id'];
                        $this->followUpTaskLabel         = $row['opvolging_taak_label'];
                        $this->followUpOrgId             = $row['opvolging_organisatie_id'];
                        $this->followUpOrgLabel          = $row['opvolging_organisatie_label'];
                        $this->followUpRolId             = $row['opvolging_rol_id'];
                        $this->followUpRolLabel          = $row['opvolging_rol_label'];
                        $this->followUpActionId          = $row['opvolging_opvolgactie_id'];
                        $this->followUpActionLabel       = $row['opvolging_opvolgactie_label'];
                        $this->followUpDate              = $row['opvolging_opvolgdatum'];
                        $this->followUpActionHolderId    = $row['opvolging_actiehouder_id'];
                        $this->followUpActionHolderLabel = $row['opvolging_actiehouder_label'];
                        $this->followUpCreatorId         = $row['opvolging_creator_id'];
                        $this->followUpCreatedOn         = $row['opvolging_created_on'];
                        $this->followUpUpdatedOn         = $row['opvolging_updated_on'];
                        $this->originalTaskLabel         = $row['original_taak_label'];
                        ?>
                        <tr data-org="<?=$this->orgId?>">
                            <td id=""><?= $this->followUpTaskLabel ?></td>
                            <td id=""><?= $this->followUpActionHolderLabel ?></td>
                            <td id=""><?= $this->followUpActionLabel ?></td>
                            <td id=""><?= date("d-m-Y", strtotime($this->followUpDate)) ?></td>
                            <td>
                            <ul class="d-flex justify-content-center">
                                <li class="followup-task mr-3"><i data-toggle="modal" data-target="#followupModal" data-id="" title="Taak op laten volgen" class="fa fa-bolt assign-task"></i></li>
                                <li class="complete-task mr-3"><i onclick="confirm('weet je het zeker?')" title="Taak afhandelen" class="fa fa-check complete-task"></i></li>
                            </ul>
                        </td>
                        </tr>
                        <?php
                       
                    }
                } else {
                    echo "Query not executed";
                }
            }



            /* Edit the user details */
            public function editUserDetails($userId)
            {
                /* Set user ID */
                $this->userID = $userId;

                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT * FROM gebruikers WHERE id = '$this->userID'";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    $this->userID           = $row['id'];
                    $this->userName         = $row['gebruikersnaam'];
                    $this->userEmail        = $row['email'];
                    $this->userPass         = $row['wachtwoord'];
                    $this->userIsAdmin      = $row['isAdmin'];
                    $this->userIsActive     = $row['isActive'];
                    $this->userIsRelManager = $row['isRelatiebeheerder'];
                    $this->userRol          = $row['rol'];
                }
                ?>
        <div class="row">
            <div class="col-lg-6 mt-3">
                <!-- Start form -->
                <form method="POST" action="controller.php?newUser" autocomplete="off">
                    <!-- Gebruikersnaam -->
                    <div class="form-group col-lg-12 mt-3">
                        <label for="username" class="col-form-label noselect">* Gebruikersnaam</label>
                        <input name="username" class="form-control" type="text" value="<?= $this->userName ?>" id="username" required>
                    </div>
                    <!-- Emailadres -->
                    <div class="form-group col-lg-12 mt-3">
                        <label for="email" class="col-form-label noselect">* Emailadres</label>
                        <input name="email" class="form-control" type="email" value="<?= $this->userEmail ?>" id="email" required>
                    </div>
                    <!-- Wachtwoord -->
                    <div class="form-group col-lg-12 mt-3">
                        <label for="password" class="col-form-label noselect">* Wachtwoord</label>
                        <input name="password" class="form-control" type="password" value="<?= $this->userPass ?>" id="password" required>
                    </div>
            </div>

            <!-- right side of form -->
            <div class="col-lg-6 mt-3">
                <div class="row">
                    <!-- Rol -->
                    <div class="form-group col-lg-12 mt-3">
                        <label for="rol" class="col-form-label noselect">* Rol</label>
                        <select class="form-control" id="rol" name="rol" required>
                            <option <?php if ($this->userRol == "sales") {
                                                echo "selected";
                                            } ?> value="sales">Sales</option>
                            <option <?php if ($this->userRol == "support") {
                                                echo "selected";
                                            } ?> value="support">Support</option>
                            <option <?php if ($this->userRol == "administratie") {
                                                echo "selected";
                                            } ?> value="administratie">Administratie</option>
                            <option <?php if ($this->userRol == "ontwikkeling") {
                                                echo "selected";
                                            } ?> value="ontwikkeling">Ontwikkeling</option>
                        </select>
                    </div>
                    <!-- IsAdmin -->
                    <div class="form-group col-lg-6 mt-3">
                        <label for="isAdmin" class="col-form-label noselect">* S4Portal Administrator</label>
                        <select class="form-control" id="isAdmin" name="isAdmin" required>
                            <option <?php if ($this->userIsAdmin === "1") {
                                                echo "selected";
                                            } ?> value="1">Ja</option>
                            <option <?php if ($this->userIsAdmin === "0") {
                                                echo "selected";
                                            } ?> value="0">Nee</option>
                        </select>
                    </div>
                    <!-- isRelatieBeheerder -->
                    <div class="form-group col-lg-6 mt-3">
                        <label for="isRelManager" class="col-form-label noselect">* Relatiebeheerder</label>
                        <select class="form-control" id="isRelManager" name="isRelManager" required>
                            <option <?php if ($this->userIsRelManager === "1") {
                                                echo "selected";
                                            } ?> value="1">Ja</option>
                            <option <?php if ($this->userIsRelManager === "0") {
                                                echo "selected";
                                            } ?> value="0">Nee</option>
                        </select>
                    </div>
                </div>
                <!-- Type of contact: Sales, Support or administration -->
                <input name="conType" type="hidden" value="sales">
                <!-- Submit button -->
                <button class="btn btn-primary btn-lg btn-block" type="submit" name="submitNewContact"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Wijziging opslaan</button>
            </div>
            </form>
            <!-- End form -->
        </div>

    <?php
        }


        /* Renders the table of all S4Portal users in "Beheer" page */
        public function renderUserEditAndDeleteTab()
        {
            ?>
        <div class="card">
            <div class="card-header">
                <a style="color:#08475b" class="card-link collapsed" data-toggle="collapse" href="#accordion22" aria-expanded="false"><i class="ti-pencil"></i>&nbsp;&nbsp;Gebruikers wijzigen/verwijderen</a>
            </div>

            <div id="accordion22" class="collapse" data-parent="#accordion2" style="">
                <div class="card-body">
                    <div class="single-table">
                        <div class="table-responsive">
                            <table id="contacts" class="table table-hover progress-table text-center">

                                <!-- Table headers -->
                                <thead class="text-uppercase">
                                    <tr>
                                        <th scope="col">Naam</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Rol</th>
                                        <th scope="col">Actief</th>
                                        <th scope="col">S4Portal Admin</th>
                                        <th scope="col">Relatiebeheerder</th>
                                        <th scope="col">Acties</th>
                                    </tr>
                                </thead>
                                <!-- Table body -->
                                <tbody>
                                    <?= $this->renderUsersTable(); ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
        </div>

        <?php }



            /* Get all country options */
            public function getAllCountryOptions()
            {

                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT * FROM tblLanden WHERE Taal = 'NL'";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {

                    $this->countryId   = $row['id'];
                    $this->countryLang = $row['Taal'];
                    $this->countryCode = $row['countryCode'];
                    $this->countryName = $row['countryName'];

                    /* Make sure current country is selected */
                    if ($this->countryName === 'Nederland') {
                        $this->countrySelected = "selected";
                    } else {
                        $this->countrySelected = "";
                    }

                    ?>
            <option <?= $this->countrySelected ?> value="<?= $this->countryName; ?>"><?= $this->countryName; ?></option>
        <?php }
            }

            /* Get all user details 


    /* Get all organisation options */
            public function getAllOrganisationOptions()
            {

                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT id,organisatienaam FROM organisatie";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                /* Check if any organisation exists (for creating new contacts)*/
                if ($stmt->rowCount() > 0) {
                    $this->orgExistence = true;
                } else {
                    $this->orgExistence = false;
                }

                while ($row = $stmt->fetch()) {
                    $this->orgId = $row['id'];
                    $this->orgName = $row['organisatienaam'];
                    ?>
            <option value="<?= $this->orgId ?>"><?= $this->orgName ?></option>
        <?php }
            }

            /* Render "nieuwe contactpersoon aanmaken" tab: SALES */
            public function renderNewContactSalesTab()
            {
                ?>
        <!-- Sales tab -->
        <div class="card">
            <div class="card-header">
                <a style="color:#08475b" class="card-link collapsed" data-toggle="collapse" href="#accordion21" aria-expanded="false"><i class='ti-credit-card'>&nbsp;</i>Sales</a>
            </div>

            <div id="accordion21" class="collapse" data-parent="#accordion2" style="">
                <div class="card-body">
                    <div class="row">

                        <!-- Organisatie -->
                        <!-- Left side of form -->
                        <div class="col-6 mt-3">

                            <!-- Start form -->
                            <form method="POST" action="controller.php?newContact" autocomplete="off">


                                <!-- Type contactpersoon -->
                                <div class="form-group">
                                    <label for="main" class="col-form-label noselect">Hoofd contactpersoon</label>
                                    <select class="form-control" id="main" name="main" required>
                                        <option value="1">Ja</option>
                                        <option selected value="0">Nee</option>
                                    </select>
                                </div>


                                <!-- Organisatie -->
                                <div class="form-group">
                                    <label for="organisation" class="col-form-label noselect">Organisatie</label>
                                    <select class="form-control" id="sex" name="organisation" required>
                                        <?= $this->getAllOrganisationOptions(); ?>
                                    </select>
                                </div>

                                <!-- Geslacht -->
                                <div class="form-group">
                                    <label for="sex" class="col-form-label noselect">Geslacht</label>
                                    <select class="form-control" id="sex" name="sex" required>
                                        <option value="man">Man</option>
                                        <option value="vrouw">Vrouw</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <!-- Voornaam -->
                                    <div class="form-group col-md-6">
                                        <label for="firstname" class="col-form-label noselect">Voornaam</label>
                                        <input name="firstname" class="form-control" type="text" value="" id="firstname" required>
                                    </div>

                                    <!-- Achternaam -->
                                    <div class="form-group col-md-6">
                                        <label for="lastname" class="col-form-label noselect">Achternaam</label>
                                        <input name="lastname" class="form-control" type="text" value="" id="lastname" required>
                                    </div>
                                </div>

                                <span style="display:inline-block;height:28px"></span>

                        </div>

                        <!-- right side of form -->
                        <div class="col-6 mt-3">

                            <!-- Functie -->
                            <div class="form-group">
                                <label for="function" class="col-form-label noselect">Functie</label>
                                <input name="function" class="form-control" type="text" value="" id="function" required>
                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <label for="email" class="col-form-label noselect">Emailadres</label>
                                <input name="email" class="form-control" type="email" value="" id="email" required>
                            </div>

                            <div class="row">
                                <!-- Telefoonnummer (vast) -->
                                <div class="form-group col-md-6">
                                    <label for="landline" class="col-form-label noselect">Telefoonnummer (vast)</label>
                                    <input name="landline" type="text" class="form-control" id="landline" value="" required>
                                </div>
                                <!-- Telefoonnummer (mobiel) -->
                                <div class="form-group col-md-6">
                                    <label for="mobile" class="col-form-label noselect">Telefoonnummer (mobiel)</label>
                                    <input name="mobile" type="text" class="form-control" id="mobile" value="">
                                </div>
                            </div>

                            <!-- Type of contact: Sales, Support or administration -->
                            <input name="conType" type="hidden" value="2">

                            <!-- Submit button -->
                            <button class="btn btn-primary btn-lg btn-block" type="submit" name="submitNewContact" <?php if (!$this->orgExistence) {
                                                                                                                                echo "disabled";
                                                                                                                            }; ?>><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Contactpersoon opslaan</button>

                        </div>

                        </form>
                        <!-- End form -->
                    </div>
                </div>
            </div>
        </div>
    <?php
        }


        /* Render "nieuwe contactpersoon aanmaken" tab: SALES */
        public function renderNewContactSupportTab()
        { ?>

        <div class="card">
            <div class="card-header">
                <a style="color:#08475b" class="card-link" data-toggle="collapse" href="#accordion22" aria-expanded="false"><i class='ti-support'>&nbsp;</i>Support</a>
            </div>

            <div id="accordion22" class="collapse" data-parent="#accordion2" style="">

                <div class="card-body">
                    <div class="row">

                        <!-- Organisatie -->
                        <!-- Left side of form -->
                        <div class="col-6 mt-3">
                            <!-- Start form -->
                            <form method="POST" action="controller.php?newContact" autocomplete="off">

                                <!-- Type contactpersoon -->
                                <div class="form-group">
                                    <label for="main" class="col-form-label noselect">Hoofd contactpersoon</label>
                                    <select class="form-control" id="main" name="main" required>
                                        <option value="1">Ja</option>
                                        <option selected value="0">Nee</option>
                                    </select>
                                </div>

                                <!-- Organisatie -->
                                <div class="form-group">
                                    <label for="organisation" class="col-form-label noselect">Organisatie</label>
                                    <select class="form-control" id="sex" name="organisation" required="">
                                        <?= $this->getAllOrganisationOptions(); ?>
                                    </select>
                                </div>

                                <!-- Geslacht -->
                                <div class="form-group">
                                    <label for="sex" class="col-form-label noselect">Geslacht</label>
                                    <select class="form-control" id="sex" name="sex" required="">
                                        <option value="man">Man</option>
                                        <option value="vrouw">Vrouw</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <!-- Voornaam -->
                                    <div class="form-group col-md-6">
                                        <label for="firstname" class="col-form-label noselect">Voornaam</label>
                                        <input name="firstname" class="form-control" type="text" value="" id="firstname" required="">
                                    </div>

                                    <!-- Achternaam -->
                                    <div class="form-group col-md-6">
                                        <label for="lastname" class="col-form-label noselect">Achternaam</label>
                                        <input name="lastname" class="form-control" type="text" value="" id="lastname" required="">
                                    </div>
                                </div>
                                <span style="display:inline-block;height:28px"></span>

                        </div>

                        <!-- right side of form -->
                        <div class="col-6 mt-3">
                            <!-- Functie -->
                            <div class="form-group">
                                <label for="function" class="col-form-label noselect">Functie</label>
                                <input name="function" class="form-control" type="text" value="" id="function" required="">
                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <label for="email" class="col-form-label noselect">Emailadres</label>
                                <input name="email" class="form-control" type="email" value="" id="email" required="">
                            </div>

                            <div class="row">
                                <!-- Telefoonnummer (vast) -->
                                <div class="form-group col-md-6">
                                    <label for="landline" class="col-form-label noselect">Telefoonnummer (vast)</label>
                                    <input name="landline" type="text" class="form-control" id="landline" value="" required="">
                                </div>

                                <!-- Telefoonnummer (mobiel) -->
                                <div class="form-group col-md-6">
                                    <label for="mobile" class="col-form-label noselect">Telefoonnummer (mobiel)</label>
                                    <input name="mobile" type="text" class="form-control" id="mobile" value="" required="">
                                </div>
                            </div>

                            <!-- Type of contact: Sales, Support or administration -->
                            <input name="conType" type="hidden" value="4">
                            <!-- Submit button -->
                            <button class="btn btn-primary btn-lg btn-block" type="submit" name="submitNewContact"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Contactpersoon opslaan</button>
                        </div>
                        </form>

                        <!-- End form -->
                    </div>
                </div>
            </div>
        </div>
    <?php
        }

        public function renderTaskHistoryModal($historyId)
        {
            $this->taskId = $historyId;
            require_once "dbconfig.php";
            $conn = DatabaseConnection::getConnection();
            $q = "SELECT * FROM opvolgingen WHERE id='$this->taskId' OR taak_id='$this->taskId' ORDER BY created_on DESC";
            $stmt = $conn->prepare($q);
            $stmt->execute();

            while ($row = $stmt->fetch()){
                $this->taskFollowUpLabel = $row['taak'];
                $this->taskFollowUpDate = date('d-m-Y', strtotime($row['opvolgdatum'])); 
                echo "<span title='Taak' class='task-history-label'>".$this->taskFollowUpLabel."&nbsp;&nbsp;<span title='Opvolgdatum' class='task-history-date'>".$this->taskFollowUpDate."</span></span><br>";
            }

        }

        /* Render "nieuwe contactpersoon aanmaken" tab: ADIMIN */
        public function renderNewContactAdminTab()
        { ?>

        <div class="card">
            <div class="card-header">
                <a style="color:#08475b" class="card-link" data-toggle="collapse" href="#accordionAdmin" aria-expanded="false"><i class="ti-agenda"></i>&nbsp;Administratie</a>
            </div>

            <div id="accordionAdmin" class="collapse" data-parent="#accordion2" style="">

                <div class="card-body">
                    <div class="row">

                        <!-- Organisatie -->
                        <!-- Left side of form -->
                        <div class="col-6 mt-3">

                            <!-- Start form -->
                            <form method="POST" action="controller.php?newContact" autocomplete="off">

                                <!-- Type contactpersoon -->
                                <div class="form-group">
                                    <label for="main" class="col-form-label noselect">Hoofd contactpersoon</label>
                                    <select class="form-control" id="main" name="main" required>
                                        <option value="1">Ja</option>
                                        <option selected value="0">Nee</option>
                                    </select>
                                </div>

                                <!-- Organisatie -->
                                <div class="form-group">
                                    <label for="organisation" class="col-form-label noselect">Organisatie</label>
                                    <select class="form-control" id="sex" name="organisation" required="">
                                        <?= $this->getAllOrganisationOptions(); ?>
                                    </select>
                                </div>

                                <!-- Geslacht -->
                                <div class="form-group">
                                    <label for="sex" class="col-form-label noselect">Geslacht</label>
                                    <select class="form-control" id="sex" name="sex" required="">
                                        <option value="man">Man</option>
                                        <option value="vrouw">Vrouw</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <!-- Voornaam -->
                                    <div class="form-group col-md-6">
                                        <label for="firstname" class="col-form-label noselect">Voornaam</label>
                                        <input name="firstname" class="form-control" type="text" value="" id="firstname" required="">
                                    </div>

                                    <!-- Achternaam -->
                                    <div class="form-group col-md-6">
                                        <label for="lastname" class="col-form-label noselect">Achternaam</label>
                                        <input name="lastname" class="form-control" type="text" value="" id="lastname" required="">
                                    </div>
                                </div>
                                <span style="display:inline-block;height:28px"></span>
                        </div>



                        <!-- right side of form -->
                        <div class="col-6 mt-3">
                            <!-- Functie -->
                            <div class="form-group">
                                <label for="function" class="col-form-label noselect">Functie</label>
                                <input name="function" class="form-control" type="text" value="" id="function" required="">
                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <label for="email" class="col-form-label noselect">Emailadres</label>
                                <input name="email" class="form-control" type="email" value="" id="email" required="">
                            </div>

                            <div class="row">
                                <!-- Telefoonnummer (vast) -->
                                <div class="form-group col-md-6">
                                    <label for="landline" class="col-form-label noselect">Telefoonnummer (vast)</label>
                                    <input name="landline" type="text" class="form-control" id="landline" value="" required="">
                                </div>
                                <!-- Telefoonnummer (mobiel) -->
                                <div class="form-group col-md-6">
                                    <label for="mobile" class="col-form-label noselect">Telefoonnummer (mobiel)</label>
                                    <input name="mobile" type="text" class="form-control" id="mobile" value="" required="">
                                </div>
                            </div>

                            <!-- Type of contact: Sales, Support or administration -->
                            <input name="conType" type="hidden" value="1">
                            <!-- Submit button -->
                            <button class="btn btn-primary btn-lg btn-block" type="submit" name="submitNewContact"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Contactpersoon opslaan</button>
                        </div>
                        </form>
                        <!-- End form -->
                    </div>
                </div>
            </div>
        </div>
    <?php
        }



        /* Render "nieuwe taak aanmaken" tab: ADMIN */
        public function renderNewTaskAdminTab()
        { ?>

        <div class="card">
            <div class="card-header">
                <a style="color:#08475b" class="card-link collapsed" data-toggle="collapse" href="#accordionSales" aria-expanded="false"><i class="ti-agenda"></i>&nbsp;&nbsp;Administratie</a>
            </div>
            <div id="accordionSales" class="collapse" data-parent="#accordion3" style="">
                <div class="card-body">
                    <div class="row-fluid">

                        <div class="form-group col-lg-12 mt-3">
                            <form action="controller.php" method="POST">

                                <!-- Taak rol -->
                                <input name="taskRol" id="taskRol" value="1" type="hidden"><br>

                                <!-- Taak title -->
                                <label for="task">Taak(omschrijving)</label>
                                <input name="task" id="task" class="form-control col-md-3" type="text" required><br>

                                <!-- Taak opvolgen boolean (ja/nee) -->
                                <label for="taskOrg">Organisatie</label>
                                <select name="taskOrg" id="taskOrg" class="form-control col-md-3" required>
                                    <?= $this->getAllOrganisationOptions() ?>
                                </select><br>

                                <!-- Taak opvolgen boolean (ja/nee) -->
                                <label for="taskFollowUp">Opvolgen</label>
                                <select name="taskFollowUp" id="taskFollowUp" class="form-control col-md-3" required>
                                    <option selected value="1">Ja</option>
                                    <option value="0">Nee</option>
                                </select><br>

                                <div class="hidableOptions">

                                    <!-- Actiehouder -->
                                    <label for="taskFollowUpHolder">Actiehouder</label>
                                    <select name="taskFollowUpHolder" class="form-control col-md-3" id="taskFollowUpHolder" required>
                                        <?= $this->getAllTaskUsers() ?>
                                    </select><br>

                                    <!-- Taak opvolgen label ( name ) -->
                                    <label for="taskFollowUpAction">Opvolgactie</label>
                                    <select name="taskFollowUpAction" class="form-control col-md-3" id="taskFollowUpAction" required>
                                        <?= $this->getAllTaskActions() ?>
                                    </select><br>

                                    <!-- Opvolgdatum -->
                                    <label for="taskFollowUpDate">Datum</label>
                                    <input name="taskFollowUpDate" class="form-control col-md-3" id="taskFollowUpDate" type="date" required>
                                </div>

                                <br>
                                <button name="createTask" type="submit" class="btn btn-primary btn-block"><i class="ti-actions"></i>&nbsp;&nbsp;Taak aanmaken</button>
                            </form>
                        </div>

                        <div class="form-group col-lg-12 mt-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php }

        public function renderNewTaskSalesTab()
        { ?>

        <div class="card">
            <div class="card-header">
                <a style="color:#08475b" class="card-link collapsed" data-toggle="collapse" href="#accordion21" aria-expanded="false"><i class="ti-credit-card"></i>&nbsp;&nbsp;Sales</a>
            </div>
            <div id="accordion21" class="collapse" data-parent="#accordion3" style="">
                <div class="card-body">
                    <div class="row-fluid">

                        <div class="form-group col-lg-12 mt-3">
                            <form action="controller.php" method="POST">

                                <!-- Taak rol -->
                                <input name="taskRol" id="taskRol" value="2" type="hidden"><br>

                                <!-- Taak title -->
                                <label for="task">Taak(omschrijving)</label>
                                <input name="task" id="task" class="form-control col-md-3" type="text" required><br>

                                <!-- Taak opvolgen boolean (ja/nee) -->
                                <label for="taskOrg">Organisatie</label>
                                <select name="taskOrg" id="taskOrg" class="form-control col-md-3" required>
                                    <?= $this->getAllOrganisationOptions() ?>
                                </select><br>

                                <!-- Taak opvolgen boolean (ja/nee) -->
                                <label for="taskFollowUp">Opvolgen</label>
                                <select name="taskFollowUp" id="taskFollowUp" class="form-control col-md-3" required>
                                    <option selected value="1">Ja</option>
                                    <option value="0">Nee</option>
                                </select><br>

                                <!-- Hidable fields if no follow up -->
                                <div class="hidableOptions">
                                    <!-- Actiehouder -->
                                    <label for="taskFollowUpHolder">Actiehouder</label>
                                    <select name="taskFollowUpHolder" class="form-control col-md-3" id="taskFollowUpHolder" required>
                                        <?= $this->getAllTaskUsers() ?>
                                    </select><br>

                                    <!-- Taak opvolgen label ( name ) -->
                                    <label for="taskFollowUpAction">Opvolgactie</label>
                                    <select name="taskFollowUpAction" class="form-control col-md-3" id="taskFollowUpAction" required>
                                        <?= $this->getAllTaskActions() ?>
                                    </select><br>

                                    <!-- Opvolgdatum -->
                                    <label for="taskFollowUpDate">Datum</label>
                                    <input name="taskFollowUpDate" class="form-control col-md-3" id="taskFollowUpDate" type="date" required>
                                </div>

                                <br>
                                <!-- Create task button-->
                                <button name="createTask" type="submit" class="btn btn-primary btn-block"><i class="ti-actions"></i>&nbsp;&nbsp;Taak aanmaken</button>
                            </form>
                        </div>

                        <div class="form-group col-lg-12 mt-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php }

        public function renderNewTaskSupportTab()
        { ?>
        <div class="card">
            <div class="card-header">
                <a style="color:#08475b" class="card-link collapsed" data-toggle="collapse" href="#accordionSupport" aria-expanded="false"><i class="ti-support"></i>&nbsp;&nbsp;Support</a>
            </div>
            <div id="accordionSupport" class="collapse" data-parent="#accordion3" style="">
                <div class="card-body">
                    <div class="row-fluid">

                        <div class="form-group col-lg-12 mt-3">
                            <form action="controller.php" method="POST">

                                <!-- Taak rol -->
                                <input name="taskRol" id="taskRol" value="4" type="hidden"><br>

                                <!-- Taak title -->
                                <label for="task">Taak(omschrijving)</label>
                                <input name="task" id="task" class="form-control col-md-3" type="text" required><br>

                                <!-- Taak opvolgen boolean (ja/nee) -->
                                <label for="taskOrg">Organisatie</label>
                                <select name="taskOrg" id="taskOrg" class="form-control col-md-3" required>
                                    <?= $this->getAllOrganisationOptions() ?>
                                </select><br>

                                <!-- Taak opvolgen boolean (ja/nee) -->
                                <label for="taskFollowUp">Opvolgen</label>
                                <select name="taskFollowUp" id="taskFollowUp" class="form-control col-md-3" required>
                                    <option selected value="1">Ja</option>
                                    <option value="0">Nee</option>
                                </select><br>

                                <div class="hidableOptions">
                                    <!-- Actiehouder -->
                                    <label for="taskFollowUpHolder">Actiehouder</label>
                                    <select name="taskFollowUpHolder" class="form-control col-md-3" id="taskFollowUpHolder" required>
                                        <?= $this->getAllTaskUsers() ?>
                                    </select><br>

                                    <!-- Taak opvolgen label ( name ) -->
                                    <label for="taskFollowUpAction">Opvolgactie</label>
                                    <select name="taskFollowUpAction" class="form-control col-md-3" id="taskFollowUpAction" required>
                                        <?= $this->getAllTaskActions() ?>
                                    </select><br>

                                    <!-- Opvolgdatum -->
                                    <label for="taskFollowUpDate">Datum</label>
                                    <input name="taskFollowUpDate" class="form-control col-md-3" id="taskFollowUpDate" type="date" required>
                                </div>

                                <br>
                                <button name="createTask" type="submit" class="btn btn-primary btn-block"><i class="ti-actions"></i>&nbsp;&nbsp;Taak aanmaken</button>
                            </form>
                        </div>

                        <div class="form-group col-lg-12 mt-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }



            /* Render contactpersonen table */
            public function renderContactpersonenTable()
            {

                //require "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT
        contactpersonen.id AS `conId`,
        contactpersonen.orgId,
        contactpersonen.geslacht,
        contactpersonen.voornaam,
        contactpersonen.achternaam,
        contactpersonen.functie,
        contactpersonen.email,
        contactpersonen.telefoon_vast,
        contactpersonen.telefoon_mobiel,
        contactpersonen.created_on,
        contactpersonen.updated_on,
        contactpersonen.type,
        contactpersonen.main,
        rollen.rol AS `type_label`,
        organisatie.organisatienaam as orgName FROM contactpersonen
        INNER JOIN rollen ON contactpersonen.type = rollen.id
        INNER JOIN organisatie ON contactpersonen.orgId = organisatie.id;";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {

                    /* Contactpersonen variables from DB */
                    $this->conId        = $row['conId'];
                    $this->conOrgId     = $row['orgId'];
                    $this->conOrgName   = $row['orgName'];
                    $this->conSex       = $row['geslacht'];
                    $this->conFirstname = $row['voornaam'];
                    $this->conLastname  = $row['achternaam'];
                    $this->conFunction  = $row['functie'];
                    $this->conEmail     = $row['email'];
                    $this->conLandline  = $row['telefoon_vast'];
                    $this->conMobile    = $row['telefoon_mobiel'];
                    $this->conType      = $row['type'];
                    $this->conMain      = $row['main'];
                    $this->conTypeLabel = $row['type_label'];
                    $this->conCreatedOn = $row['created_on'];
                    $this->conUpdatedOn = $row['updated_on'];

                    // Format type contactpersoon
                    if ($this->conMain === "1") {
                        $this->conMain = "Hoofd contactpersoon";
                    } else {
                        $this->conMain = "=";
                    }

                    // Format create date if never used
                    if ($this->conCreatedOn === "0000-00-00 00:00:00") {
                        $this->conCreatedOn = "-";
                    }

                    // Format update date if never used
                    if ($this->conUpdatedOn === "0000-00-00 00:00:00") {
                        $this->conUpdatedOn = "-";
                    }
                    ?>

            <tr>
                <td><a href="organisatie.php?id=<?= $this->conOrgId ?>"><?= $this->conOrgName ?></a></td>
                <td><?= ucfirst($this->conTypeLabel); ?></td>
                <td><?= $this->conFirstname ?></td>
                <td><?= $this->conLastname ?></td>
                <td><?= $this->conFunction ?></td>
                <td><a href="mailto:<?= $this->conEmail ?>"><?= $this->conEmail ?></a></td>
                <td><a href="tel:<?= $this->conLandline ?>"><?= $this->conLandline ?></a></td>
                <td><a href="tel:<?= $this->conMobile ?>"><?= $this->conMobile ?></a></td>
                <td><?= $this->conMain ?></td>
                <td>
                    <ul class="d-flex justify-content-center">
                        <li class="mr-3"><a href="#" class="text-secondary"><i data-conid="<?= $this->conId ?>" data-conorg="<?= $this->conOrgName ?>" data-conorgid="<?= $this->conOrgId ?>" data-consex="<?= $this->conSex ?>" data-confirstname="<?= $this->conFirstname ?>" data-conlastname="<?= $this->conLastname ?>" data-confunction="<?= $this->conFunction ?>" data-conemail="<?= $this->conEmail ?>" data-conlandline="<?= $this->conLandline ?>" data-conmobile="<?= $this->conMobile ?>" data-contype="<?= $this->conType ?>" data-contypeid="<?= $this->conTypeId ?>" data-toggle="modal" data-target="#contactModal" class="fa fa-edit edit-contact"></i></a></li>
                        <li><a href="#" class="text-danger"><i class="ti-trash"></i></a></li>
                    </ul>
                </td>
            </tr>

        <?php }
            }


            /* Render the users table */
            public function renderUsersTable()
            {

                include_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT 
        gebruikers.id,
        gebruikers.gebruikersnaam,
        gebruikers.email,
        gebruikers.wachtwoord,
        gebruikers.isAdmin,
        gebruikers.isActive,
        gebruikers.isRelatiebeheerder,
        gebruikers.rol,
        rollen.rol AS 'userRoleName'
        FROM gebruikers INNER JOIN rollen ON gebruikers.rol = rollen.id WHERE gebruikers.bot = 0 ORDER BY gebruikers.gebruikersnaam ASC, gebruikers.isActive DESC, gebruikers.isAdmin DESC, rollen.rol DESC, gebruikers.isRelatiebeheerder DESC;";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    $this->userID           = $row['id'];
                    $this->userName         = $row['gebruikersnaam'];
                    $this->userEmail        = $row['email'];
                    $this->userPass         = $row['wachtwoord'];
                    $this->userIsAdmin      = $row['isAdmin'];
                    $this->userIsActive     = $row['isActive'];
                    $this->userIsRelManager = $row['isRelatiebeheerder'];
                    $this->userRol          = $row['rol'];
                    $this->userRoleName     = $row['userRoleName'];

                    /* Check if user is admin and show image instead of text */
                    if ($this->userIsAdmin === "1") {
                        $userIsAdminIcon = "<img title='Ja' width='15' src='assets/images/icon/yes.png'>";
                    } else if ($this->userIsAdmin === "0") {
                        $userIsAdminIcon = "<img title='Nee' width='15' src='assets/images/icon/no.png'>";
                    } else {
                        $userIsAdminIcon = "Niet herkend";
                    }

                    /* Check if user is active and show image instead of text */
                    if ($this->userIsActive === "1") {
                        $userIsActiveIcon = "<img title='Ja' width='15' src='assets/images/icon/yes.png'>";
                    } else if ($this->userIsActive === "0") {
                        $userIsActiveIcon = "<img title='Nee' width='15' src='assets/images/icon/no.png'>";
                    } else {
                        $userIsActiveIcon = "Niet herkend";
                    }

                    /* Check if user is relatiebeheerder and show image instead of text */
                    if ($this->userIsRelManager === "1") {
                        $userIsRelManagerIcon = "<img title='Ja' width='15' src='assets/images/icon/yes.png'>";
                    } else if ($this->userIsRelManager === "0") {
                        $userIsRelManagerIcon = "<img title='Nee' width='15' src='assets/images/icon/no.png'>";
                    } else {
                        $userIsRelManagerIcon = "Niet herkend";
                    }

                    ?>
            <tr data-row-id="<?= $this->userID ?>" id="<?= $this->userID ?>">
                <td id="editUsername"><?= $this->userName ?></td>
                <td id="editEmail"><?= $this->userEmail ?></td>
                <td id="editRol"><?= $this->userRoleName ?></td>
                <td id="editIsActive"><?= $userIsActiveIcon ?></td>
                <td id="editIsAdmin"><?= $userIsAdminIcon ?></td>
                <td id="editIsRelManager"><?= $userIsRelManagerIcon ?></td>
                <td>
                    <ul class="d-flex justify-content-center">
                        <?php if (!$this->userIsAdmin) { ?>
                            <li data-userid="<?= $this->userID ?>" data-username="<?= $this->userName ?>" data-pass="<?= $this->userPass ?>" data-email="<?= $this->userEmail ?>" data-isadmin="<?= $this->userIsAdmin ?>" data-isactive="<?= $this->userIsActive ?>" data-isrelmanager="<?= $this->userIsRelManager ?>" data-rol="<?= $this->userRol ?>" class="edit-user mr-3"><i data-toggle="modal" data-target="#exampleModalLong" data-id="<?= $this->userID ?>" title="Gebruikersgegevens wijzigen" class="fa fa-edit edit-user"></i></li>
                            <li><a onClick="return confirm('Gebruiker verwijderen?')" href="controller.php?deleteUser=<?= $this->userID ?>" class="text-danger"><i title="Gebruiker verwijderen" class="ti-trash del-user"></i></a></li>
                        <?php } ?>
                    </ul>
                </td>
            </tr>
        <?php
                }
            }

            /* Create the organisations table */
            public function renderOrganisationTable()
            {
                include_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT organisatie.id AS `org_id`,
        organisatie.organisatienummer AS `org_num`,
        organisatie.organisatienaam AS `org_name`,
        organisatie.adres AS `org_address`,
        organisatie.huisnummer AS `org_housenr`,
        organisatie.postcode AS `org_postal`,
        organisatie.plaats AS `org_city`,
        organisatie.land AS `org_country`,
        organisatie.telefoon AS `org_phone`,
        organisatie.email AS `org_email`,
        organisatie.notities AS `org_notes`,
        organisatie.erp AS `org_erp`,
        organisatie.cms AS `org_cms`,
        organisatie.kredietverzekeraar AS `org_ins`,
        organisatie.informatiebureau AS `org_inf`,
        organisatie.incassopartner AS `org_incasso`,
        organisatie.s4_product AS `org_s4product`,
        organisatie.relatiebeheerder AS `org_rel`,
        organisatie.status AS `org_status`,
        organisatie.created_on AS `org_createdon`,
        organisatie.updated_on AS `org_updatedon`,
        gebruikers.gebruikersnaam AS `usr_username`,
        s4producten.name AS `org_s4product_name`
        FROM organisatie
        INNER JOIN gebruikers ON organisatie.relatiebeheerder = gebruikers.id
        INNER JOIN s4producten ON s4producten.id = organisatie.s4_product";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {

                    $this->orgId             = $row['org_id'];
                    $this->orgNumber         = $row['org_num'];
                    $this->orgName           = $row['org_name'];
                    $this->orgAddress        = $row['org_address'];
                    $this->orgHouseNumber    = $row['org_housenr'];
                    $this->orgPostalCode     = $row['org_postal'];
                    $this->orgCity           = $row['org_city'];
                    $this->orgCountry        = $row['org_country'];
                    $this->orgPhone          = $row['org_phone'];
                    $this->orgEmail          = $row['org_email'];
                    $this->orgNotes          = $row['org_notes'];
                    $this->orgERP            = $row['org_erp'];
                    $this->orgCMS            = $row['org_cms'];
                    $this->orgIns            = $row['org_ins'];
                    $this->orgInfDesk        = $row['org_inf'];
                    $this->orgIncassoPartner = $row['org_incasso'];
                    $this->orgS4Prod         = $row['org_s4product'];
                    $this->orgS4ProdName     = $row['org_s4product_name'];
                    $this->orgRelName        = $row['usr_username'];
                    $this->orgRelId          = $row['org_rel'];
                    $this->orgStatus         = $row['org_status'];
                    $this->orgCreatedOn      = $row['org_createdon'];
                    $this->orgUpdatedOn      = $row['org_updatedon'];

                    /* Check last updated */
                    if ($this->orgUpdatedOn === "0000-00-00 00:00:00") {
                        $this->orgUpdatedOn = "-";
                    }

                    /* Assign icons to type of customers */

                    /* Inactieve klant */
                    if ($this->orgStatus === "1") {
                        $this->orgStatusIcon = "<img title='Inactieve klant' width='20' src='assets/images/icon/inactieve-klant.png'><span style='display:none;'>Inactief</span>";
                    }
                    /* Prospect */ elseif ($this->orgStatus === "3") {
                        $this->orgStatusIcon = "<img title='Prospect' width='20' src='assets/images/icon/prospect.png'><span style='display:none;'>Prospect</span>";
                    }

                    /* Klant */ elseif ($this->orgStatus === "4") {
                        $this->orgStatusIcon = "<img title='Klant' width='20' src='assets/images/icon/klant.png'><span style='display:none;'>Klant</span>";
                    }
                    /* Klant */ else {
                        $this->orgStatusIcon = "<img title='Suspect' width='20' src='assets/images/icon/suspect.png'><span style='display:none;'>Klant</span>";
                    }

                    ?>
            <tr>
                <td><?= $this->orgStatusIcon ?></td>
                <td><a href="organisatie.php?id=<?= $this->orgId ?>"><?= $this->orgName ?></a></td>
                <td><?= $this->orgRelId ?></td>
                <td><?= $this->orgCreatedOn ?></td>
                <td>
                    <ul class="d-flex justify-content-center">
                        <li data-orgid="<?= $this->orgId ?>" data-orgnum="<?= $this->orgNumber ?>" data-orgname="<?= $this->orgName ?>" data-orgaddress="<?= $this->orgAddress ?>" data-orghousenr="<?= $this->orgHouseNumber ?>" data-orgpostal="<?= $this->orgPostalCode ?>" data-orgcity="<?= $this->orgCity ?>" data-orgcountry="<?= $this->orgCountry ?>" data-orgphone="<?= $this->orgPhone ?>" data-orgemail="<?= $this->orgEmail ?>" data-orgstatus="<?= $this->orgStatus ?>" data-orgnotes="<?= $this->orgNotes ?>" data-orgerp="<?= $this->orgERP ?>" data-orgcms="<?= $this->orgCMS ?>" data-orgins="<?= $this->orgIns ?>" data-orginf="<?= $this->orgInfDesk ?>" data-orgincasso="<?= $this->orgIncassoPartner ?>" data-orgs4prod="<?= $this->orgS4Prod ?>" data-orgs4prodname="<?= $this->orgS4ProdName ?>" data-orgrel="<?= $this->orgRelId ?>" data-orgrelname="<?= $this->orgRelName ?>" class="edit-org mr-3"><i data-toggle="modal" data-target="#orgModal" data-id="<?= $this->orgRelId ?>" title="Organisatiegegevens wijzigen" class="fa fa-edit edit-org"></i></li>
                        <li><a onClick="return confirm('Organisatie verwijderen?')" href="controller.php?deleteOrg=<?= $this->orgId ?>" class="text-danger"><i title="Organisatie verwijderen" class="ti-trash del-org"></i></a></li>
                    </ul>
                </td>
            </tr>
        <?php }
            }

            /* VIEW: organisation (Set all variables from DB) */
            public function viewOrganisation($orgId)
            {
                $this->orgId = $orgId;

                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "SELECT 
        contactpersonen.id AS `con_id`,
        contactpersonen.orgId AS `con_org_id`,
	    organisatie.organisatienummer AS `org_num`,
        organisatie.organisatienaam AS `org_name`,
        organisatie.adres AS `org_adres`,
        organisatie.huisnummer AS `org_housenumber`,
        organisatie.postcode AS `org_postalcode`,
        organisatie.plaats AS `org_city`,
        organisatie.land AS `org_country`,
        organisatie.telefoon AS `org_tel`,
        organisatie.email AS `org_email`,
        organisatie.notities AS `org_notes`,
        organisatie.erp AS `org_erp`,
        organisatie.cms AS `org_cms`,
        organisatie.kredietverzekeraar AS `org_ins`,
        organisatie.informatiebureau AS `org_inf`,
        organisatie.incassopartner AS `org_incasso`,
        organisatie.s4_product AS `org_s4product_id`,
        s4producten.name AS `org_s4product_name`,
        organisatie.relatiebeheerder AS `org_rel_id`,
        gebruikers.gebruikersnaam AS `org_rel_name`,
        organisatie.creator AS `org_creator_id`,
        gebruikers.gebruikersnaam AS `org_creator_name`,
        organisatie.created_on AS `org_createdon`,
        organisatie.updated_on AS `org_updatedon`,
        contactpersonen.geslacht AS `con_sex`,
        contactpersonen.voornaam AS `con_firstname`,
        contactpersonen.achternaam AS `con_lastname`,
        contactpersonen.functie AS `con_function`,
        contactpersonen.email AS `con_email`,
        contactpersonen.main AS `con_main`,
        contactpersonen.telefoon_vast AS `con_landline`,
        contactpersonen.telefoon_mobiel AS `con_mobile`,
        contactpersonen.created_on AS `con_createdon`,
        contactpersonen.updated_on AS `con_updatedon`,
        organisatie.status AS `customer_type_id`,
        klantsoorten.name AS `customer_type_name`
        FROM organisatie
        INNER JOIN gebruikers ON organisatie.relatiebeheerder = gebruikers.id
        INNER JOIN s4producten ON s4producten.id = organisatie.s4_product
        INNER JOIN klantsoorten ON klantsoorten.id = organisatie.status
        LEFT JOIN contactpersonen on contactpersonen.orgId = organisatie.id
        WHERE organisatie.id='$this->orgId' ORDER BY contactpersonen.main DESC";
                $stmt = $conn->prepare($q);
                $stmt->execute();

                while ($row = $stmt->fetch()) {
                    if ($row['con_main'] == "1") {
                        $this->conId             = $row['con_id'];
                        $this->conOrgId          = $row['con_org_id'];
                        $this->conMain           = $row['con_main'];
                        $this->conFirstname      = $row['con_firstname'];
                        $this->conLastname       = $row['con_lastname'];
                        $this->conFunction       = $row['con_function'];
                        $this->conEmail          = $row['con_email'];
                        $this->conLandline       = $row['con_landline'];
                        $this->conMobile         = $row['con_mobile'];
                        $this->orgStatus         = $row['customer_type_id'];
                        $this->conStatusName     = $row['customer_type_name'];
                        $this->conSex            = $row['con_sex'];
                        $this->conCreatedOn      = date("d-m-Y", strtotime($row['con_createdon']));
                        $this->conUpdatedOn      = date("d-m-Y", strtotime($row['con_updatedon']));
                        $this->orgNumber         = $row['org_num'];
                        $this->orgName           = $row['org_name'];
                        $this->orgAddress        = $row['org_adres'];
                        $this->orgHouseNumber    = $row['org_housenumber'];
                        $this->orgPostalCode     = $row['org_postalcode'];
                        $this->orgCity           = $row['org_city'];
                        $this->orgCountry        = $row['org_country'];
                        $this->orgPhone          = $row['org_tel'];
                        $this->orgEmail          = $row['org_email'];
                        $this->orgNotes          = $row['org_notes'];
                        $this->orgERP            = $row['org_erp'];
                        $this->orgCMS            = $row['org_cms'];
                        $this->orgIns            = $row['org_ins'];
                        $this->orgInfDesk        = $row['org_inf'];
                        $this->orgIncassoPartner = $row['org_incasso'];
                        $this->orgS4Prod         = $row['org_s4product_id'];
                        $this->orgS4ProdName     = $row['org_s4product_name'];
                        $this->orgNotes          = $row['org_notes'];
                        $this->orgRelId          = $row['org_rel_id'];
                        $this->orgRelName        = $row['org_rel_name'];
                        $this->orgCreator        = $row['org_creator_name'];
                        $this->orgCreatedOn      = $row['org_createdon'];
                        $this->orgUpdatedOn      = $row['org_updatedon'];
                    } else {
                        $this->orgStatus         = $row['customer_type_id'];
                        $this->conStatusName     = $row['customer_type_name'];
                        $this->orgNumber         = $row['org_num'];
                        $this->orgName           = $row['org_name'];
                        $this->orgAddress        = $row['org_adres'];
                        $this->orgHouseNumber    = $row['org_housenumber'];
                        $this->orgPostalCode     = $row['org_postalcode'];
                        $this->orgCity           = $row['org_city'];
                        $this->orgCountry        = $row['org_country'];
                        $this->orgPhone          = $row['org_tel'];
                        $this->orgEmail          = $row['org_email'];
                        $this->orgNotes          = $row['org_notes'];
                        $this->orgERP            = $row['org_erp'];
                        $this->orgCMS            = $row['org_cms'];
                        $this->orgIns            = $row['org_ins'];
                        $this->orgInfDesk        = $row['org_inf'];
                        $this->orgIncassoPartner = $row['org_incasso'];
                        $this->orgS4Prod         = $row['org_s4product_id'];
                        $this->orgS4ProdName     = $row['org_s4product_name'];
                        $this->orgNotes          = $row['org_notes'];
                        $this->orgRelId          = $row['org_rel_id'];
                        $this->orgRelName        = $row['org_rel_name'];
                        $this->orgCreator        = $row['org_creator_name'];
                        $this->orgCreatedOn      = $row['org_createdon'];
                        $this->orgUpdatedOn      = $row['org_updatedon'];
                    }
                }
            }

            /* Function that checks if user has access to the page ( is admin or not ) and redirects back to index if no admin */
            public function restrictAccess()
            {
                if ($_SESSION['isAdmin'] !== "1") {
                    header("Location: index.php");
                }
            }

            /* Delete a file / attachment */
            public function deleteFile($fileId, $fileName)
            {

                $this->fileId   = $fileId;    // File ID
                $this->fileName = $fileName;  // File name

                /* Upload directory */
                $uploadDir = "uploads" . "/";
                /* Full path */
                $fullPath = $uploadDir . $this->fileName;

                /* Check if file exists before deleting */
                if (file_exists($fullPath)) {

                    /* Check the rights to write before trying to delete */
                    if (is_writable($fullPath)) {

                        /* Delete file registration from database */
                        include_once "dbconfig.php";
                        $conn = DatabaseConnection::getConnection();
                        $q = "DELETE FROM `bijlagen` WHERE id='$this->fileId'";
                        $stmt = $conn->prepare($q);

                        /* If query executed */
                        if ($stmt->execute()) {

                            /* Delete file from server */
                            if (unlink($fullPath)) {
                                header("Location: bijlagen.php");
                            }

                            /* If query not executed */
                        } else {
                            die("Bestand kon niet worden verwijderd");
                        }
                    };

                    /* File does not exist */
                } else {
                    include_once "dbconfig.php";
                    $conn = DatabaseConnection::getConnection();
                    $q = "DELETE FROM `bijlagen` WHERE `name`='$this->fileName'";
                    $stmt = $conn->prepare($q);
                    if ($stmt->execute()) {
                        echo "Bestand niet gevonden op de server of is al reeds verwijderd.";
                        exit();
                    } else {
                        echo "Database fout: kon bestand niet vinden of verwijderen";
                    }
                }
            }

            /* Delete S4Portal user */
            public function deleteUser($userId)
            {
                $this->userID = $userId;
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "DELETE FROM `gebruikers` WHERE `id`='$this->userID' AND isAdmin=0";
                $stmt = $conn->prepare($q);
                if ($stmt->execute()) {
                    // Gebruiker verwijderd
                    header("Location: beheer.php");
                } else {
                    // Gebruiker niet verwijderd
                    header("Location: beheer.php");
                }
            }

            /* Delete organisation */
            public function deleteOrganisation($orgId)
            {
                $this->orgId = $orgId;
                require_once "dbconfig.php";
                $conn = DatabaseConnection::getConnection();
                $q = "DELETE FROM `organisatie` WHERE `id`='$this->orgId'";
                $stmt = $conn->prepare($q);
                if ($stmt->execute()) {
                    // Organisatie verwijderd
                    header("Location: organisaties.php");
                } else {
                    // Organisatie niet verwijderd
                    header("Location: organisaties.php");
                }
            }

            /* Create new organisation and save it into the database */
            public function createNewOrganisation(
                $orgId,
                $orgNumber,
                $orgName,
                $orgAddress,
                $orgHouseNumber,
                $orgPostalCode,
                $orgCity,
                $orgCountry,
                $orgPhone,
                $orgEmail,
                $orgStatus,
                $orgNotes,
                $orgERP,
                $orgCMS,
                $orgIns,
                $orgInfDesk,
                $orgIncassoPartner,
                $orgS4Prod,
                $orgRel,
                $userID
            ) {
                $this->orgId             = $orgId;
                $this->orgNumber         = $orgNumber;
                $this->orgName           = $orgName;
                $this->orgAddress        = $orgAddress;
                $this->orgHouseNumber    = $orgHouseNumber;
                $this->orgPostalCode     = $orgPostalCode;
                $this->orgCity           = $orgCity;
                $this->orgCountry        = $orgCountry;
                $this->orgPhone          = $orgPhone;
                $this->orgEmail          = $orgEmail;
                $this->orgStatus         = $orgStatus;
                $this->orgNotes          = $orgNotes;
                $this->orgERP            = $orgERP;
                $this->orgCMS            = $orgCMS;
                $this->orgIns            = $orgIns;
                $this->orgInfDesk        = $orgInfDesk;
                $this->orgIncassoPartner = $orgIncassoPartner;
                $this->orgS4Prod         = $orgS4Prod;
                $this->orgRel            = $orgRel;
                $this->userID            = $userID;

                /* Insert into ORGANISATIONS table */
                require_once('dbconfig.php');
                $conn = DatabaseConnection::getConnection();
                $q = "INSERT INTO organisatie(
            organisatienummer,
            organisatienaam,
            adres,
            huisnummer,
            postcode,
            plaats,
            land,
            telefoon,
            email,
            status,
            notities,
            erp,
            cms,
            kredietverzekeraar,
            informatiebureau,
            incassopartner,
            s4_product,
            relatiebeheerder,
            creator
            ) VALUES(
                '$this->orgNumber',
                '$this->orgName',
                '$this->orgAddress',
                '$this->orgHouseNumber',
                '$this->orgPostalCode',
                '$this->orgCity',
                '$this->orgCountry',
                '$this->orgPhone',
                '$this->orgEmail',
                '$this->orgStatus',
                '$this->orgNotes',
                '$this->orgERP',
                '$this->orgCMS',
                '$this->orgIns',
                '$this->orgInfDesk',
                '$this->orgIncassoPartner',
                '$this->orgS4Prod',
                '$this->orgRel',
                '$this->userID'
                 )";
                $stmt = $conn->prepare($q);

                if ($stmt->execute()) {

                    /* Create notification */
                    $title      = "Nieuwe organisatie";                                                                          // Notification title
                    $content    = 'Organisatie <span class="n-orgname">' . $this->orgName . '</span> is aangemaakt!';                           // Notification content
                    $iconID     = 1;                                                                                                        // Normal notification
                    $senderID   = $_SESSION['user_id'];                                                                                     // S4Portal Support Bot
                    $receiverID = 36;                                                                                                       // Notification receiver

                    require_once "notification_system/notification.class.php";
                    $Notification = new Notification();
                    $Notification->createNotification($title, $content, $iconID, $senderID, $receiverID);

                    /* Redirect user to Organisaties.php */
                    header("Location: organisaties.php");
                } else {
                    header("Location: organisaties.php");
                }
            }

            /* Create new contactpersoon and save into database */
            public function createNewContact($orgId, $conSex, $conFirstname, $conLastname, $conFunction, $conEmail, $conLandline, $conMobile, $conType, $conMain)
            {

                $this->orgId        = $orgId;
                $this->conSex       = $conSex;
                $this->conFirstname = $conFirstname;
                $this->conLastname  = $conLastname;
                $this->conFunction  = $conFunction;
                $this->conEmail     = $conEmail;
                $this->conLandline  = $conLandline;
                $this->conMobile    = $conMobile;
                $this->conType      = $conType;
                $this->conMain      = $conMain;

                require_once('dbconfig.php');
                $conn = DatabaseConnection::getConnection();
                $q = "INSERT INTO contactpersonen (orgId, geslacht, voornaam, achternaam, functie, email, telefoon_vast, telefoon_mobiel, type, main) VALUES('$this->orgId', '$this->conSex', '$this->conFirstname', '$this->conLastname', '$this->conFunction','$this->conEmail', '$this->conLandline', '$this->conMobile', '$this->conType', '$this->conMain')";
                $stmt = $conn->prepare($q);
                if ($stmt->execute()) {
                    header("Location: contactpersonen.php?success=newContact");
                } else {
                    header("Location: contactpersonen.php?error=newContact");
                }
            }

            /* Edit contactpersoon and save into database */
            public function editContact($conId, $conOrg, $conType, $conSex, $conFirstName, $conLastName, $conFunction, $conEmail, $conLandLine, $conMobile)
            {
                $this->conId        = $conId;
                $this->conOrgId     = $conOrg;
                $this->conType      = $conType;
                $this->conSex       = $conSex;
                $this->conFirstName = $conFirstName;
                $this->conLastName  = $conLastName;
                $this->conFunction  = $conFunction;
                $this->conEmail     = $conEmail;
                $this->conLandline  = $conLandLine;
                $this->conMobile    = $conMobile;

                require_once('dbconfig.php');
                $conn = DatabaseConnection::getConnection();
                $q = "UPDATE contactpersonen SET
                `orgId` = '$this->conOrgId',
                `geslacht` = '$this->conSex',
                `voornaam` = '$this->conFirstName',
                `achternaam` = '$this->conLastName',
                `functie` = '$this->conFunction',
                `email` = '$this->conEmail',
                `telefoon_vast` = '$this->conLandline',
                `telefoon_mobiel` = '$this->conMobile',
                `type` = '$this->conType'
                WHERE id='$this->conId'";

                $stmt = $conn->prepare($q);
                if ($stmt->execute()) {
                    header("Location: contactpersonen.php?success=editContact");
                } else {
                    header("Location: contactpersonen.php?error=editContact");
                }
            }

            /* Create new task and save into database */
            public function createNewTask($task, $taskRol, $taskFollowUpAction, $taskFollowUpHolder, $taskFollowUpDate, $taskCreator, $taskOrg, $taskFollowUpBoolean)
            {

                $this->taskContent        = $task;                // Task content
                $this->taskOrg            = $taskOrg;             // Organisation of task
                $this->taskRol            = $taskRol;             // Role of task ( sales , support etc )

                $this->taskFollowUpAction = $taskFollowUpAction;  // Task Action
                $this->taskFollowUpUser   = $taskFollowUpHolder;  // Task holder/user
                $this->taskFollowUpDate   = $taskFollowUpDate;    // Task Follow up date

                /* TASK WITH FOLLOWUP */
                if ($this->taskFollowUpAction !== 'NULL'){
                    $this->taskCreatorId      = $taskCreator;         // Task creator
                    $this->taskFollowUp       = $taskFollowUpBoolean; // Task follow up boolean

                    require_once('dbconfig.php');
                    $conn = DatabaseConnection::getConnection();
                    $q    = "INSERT INTO taken (taak, organisatie, rol, opvolgactie, opvolgdatum, actiehouder, creator, heeft_opvolging)
                    VALUES('$this->taskContent', '$this->taskOrg','$this->taskRol', '$this->taskFollowUpAction', '$this->taskFollowUpDate', '$this->taskFollowUpUser', '$this->taskCreatorId', 1)";
                    $stmt = $conn->prepare($q);

                    /* Create notification if task inserted */
                    if ($stmt->execute()) {

                        /* Copy ID to taak ID if its a first task (with no follow-ups yet) */
                        
                        $conn = DatabaseConnection::getConnection();
                        $q = "UPDATE opvolgingen SET taak_id = '$last_id' WHERE id='$last_id'";
                        $stmt = $conn->prepare($q);
                        $stmt->execute();

                        $title      = "Nieuwe taak aangemaakt";         // Notification title
                        $content    = $this->taskContent;               // Notification content
                        $iconID     = 2;                                // Important icon
                        $senderID   = 53;                               // S4Portal Support
                        $receiverID = $this->taskCreatorId;

                        require_once "notification_system/notification.class.php";
                        $Notification = new Notification();
                        $Notification->createNotification($title, $content, $iconID, $senderID, $receiverID);

                        header("Location: taken.php?success=newTask");
                    } else {
                        header("Location: taken.php?error=newTask");
                    }
                    
                    /* TASK WITH NO FOLLOWUP */
                } else {
                    $this->taskCreatorId      = $taskCreator;         // Task creator
                    $this->taskFollowUp       = $taskFollowUpBoolean; // Task follow up boolean

                    require_once('dbconfig.php');
                    $conn = DatabaseConnection::getConnection();
                    $q    = "INSERT INTO taken (taak, organisatie, rol, opvolgactie, opvolgdatum, actiehouder, creator, heeft_opvolging,afgehandeld)
                    VALUES('$this->taskContent', '$this->taskOrg','$this->taskRol', NULL, NULL, NULL, '$this->taskCreatorId',0,1)";
                    $stmt = $conn->prepare($q);

                    /* Create notification if task inserted */
                    if ($stmt->execute()) {

                        $title      = "Nieuwe taak";                    // Notification title
                        $content    = `Er is een nieuwe taak gemaakt`;  // Notification content
                        $iconID     = 2;                                // Important icon
                        $senderID   = 53;                               // S4Portal Support
                        $receiverID = $this->taskCreatorId;

                        require_once "notification_system/notification.class.php";
                        $Notification = new Notification();
                        $Notification->createNotification($title, $content, $iconID, $senderID, $receiverID);

                        header("Location: taken.php?success=newTask");
                    } else {
                        header("Location: taken.php?error=newTask");
                    }
                }
            }

            /* Follow up a task */
            public function followUpTask(
                $originalTaskId,
                $taskContent,
                $taskOrgId,
                $taskRol,
                $taskFollowUpActionID,
                $taskFollowupHolderID,
                $taskFollowupCreatorID,
                $taskFollowupDate)
            {
                $this->taskOrgId            = $taskOrgId;              // ID of organisation of task
                $this->taskId               = $originalTaskId;         // ID of the original task
                $this->taskRol              = $taskRol;                // ID of role of task ( sales, support etc)
                $this->taskContent          = $taskContent;            // The content of the task
                $this->taskCreatorId        = $taskFollowupCreatorID;  // The ID of user who created this follow-up task
                $this->taskFollowUpUser     = $taskFollowupHolderID;   // The ID of user who is the owner/actionholder of this task
                $this->taskFollowUpActionId = $taskFollowUpActionID;   // The ID of the follow up action
                $this->taskFollowUpDate     = $taskFollowupDate;       // The task follow-up date ( not creation date )
                require_once('dbconfig.php');

                /* If first follow up task */
                    $conn = DatabaseConnection::getConnection();
                    $q    = "INSERT INTO opvolgingen (taak_id, taak, organisatie, rol, opvolgactie, opvolgdatum, actiehouder, creator)
                    VALUES(
                        '$this->taskId',
                        '$this->taskContent',
                        $this->taskOrgId,
                        $this->taskRol,
                        $this->taskFollowUpActionId,
                        '$this->taskFollowUpDate',
                        $this->taskFollowUpUser,
                        $this->taskCreatorId)";
    
                    $stmt = $conn->prepare($q);

                    if ($stmt->execute()) {
                        header("Location: http://mijnprojecten.ml/s4portal/modash/organisatie.php?id=".$this->taskOrgId);
                    } else {
                        header("Location: http://mijnprojecten.ml/s4portal/modash/organisatie.php?id=".$this->taskOrgId);
                    }
            }

            /* Render Sales Organisation view (inside company view) */
            public function renderSalesOrganisationView()
            { ?>
                        <!-- ASSIGN TASK Modal --> 
                        <form id="assignTask" action="controller.php" method="POST">
                        <div class="modal fade" id="tasksModal" aria-hidden="true" style="display:none">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title noselect"><i class="ti-pencil-alt"></i>&nbsp;&nbsp;<span class="taskDesc"></span> laten opvolgen</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Taak ID (ID van oorspronkelijke taak waarin deze wordt opgevolgd, Hidden)-->
                                        <input class="originalTaskId" type="hidden" name="originalTaskId" value="">

                                        <!-- Taak ID ( ID van deze taak ) -->
                                        <input class="taskId" type="hidden" name="taskId" value="">

                                        <!-- Taak (content) -->
                                        <label for="taskContent">Taak omschrijving</label>
                                        <input id="taskContent" class="taskContent form-control" type="text" name="taskContent" value=""><br>

                                        <!-- Organisatie ID (Hidden)-->
                                        <input class="taskOrgId" type="hidden" name="taskOrgId" value="">

                                         <!-- Taak rol -->
                                         <input class="taskRolId" type="hidden" name="taskRolId" value="<?=$_SESSION['userRol']?>">

                                        <!-- Task creation date -->
                                        <input class="taskCreationDate" name="taskCreationDate" type="hidden" value="">

                                        <!-- Task owner ID -->
                                        <input class="taskCreatorId" name="taskCreatorId" type="hidden" value="<?=$_SESSION['user_id']?>">
                                        
                                        <!-- Actiehouder -->
                                        <label for="taskFollowupHolderId">Actiehouder</label>
                                        <select name="taskFollowupHolderId" class="taskFollowupHolderId form-control" id="taskFollowupHolderId" required>
                                            <?= $this->getAllTaskUsers()?>
                                        </select>
                                        <br>
                                        <!-- Opvolgactie --> 
                                        <label for="taskFollowupActionId">Opvolgactie</label>
                                        <select name="taskFollowupActionId" class="taskLabel form-control" id="taskFollowupActionId" required>
                                            <?=$this->getAllTaskActions()?>
                                        </select>
                                        <br>
                                        <!-- Opvolgdatum -->
                                        <label for="taskFollowupDate">Opvolgdatum</label>
                                        <input type="date" name="taskFollowupDate" id="taskFollowupDate" class="taskFollowupDate form-control" required>
                                    </div>

                                    <!-- Modal buttons (Save and close) -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                        <button type="submit" name="followUpTask" class="btn btn-primary">Taak opvolgen</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>


                    <!-- TASK HISTORY modal -->
                    <div class="modal fade" id="historyModal" aria-hidden="true" style="display:none">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title noselect"><i class="ti-book"></i>&nbsp;&nbsp;Eerdere opvolgingen</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                                    </div>
                                    <div class="modal-body task-history-body">
                                        
                                    </div>
                                    <!-- Modal buttons (Save and close) -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                        <!-- <button type="submit" name="followUpTask" class="btn btn-primary">Taak opvolgen</button> -->
                                    </div>

                                </div>
                            </div>
                        </div>



        <div class="row">

            <div class="col-lg-12 mt-3">

                <div class="card">
                    <div class="card-body">

                        <!-- Tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" id="bedrijf-tab" data-toggle="tab" href="#bedrijf" role="tab" aria-controls="bedrijf" aria-selected="true">Bedrijfsgegevens</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="systeem-tab" data-toggle="tab" href="#systeem" role="tab" aria-controls="systeem" aria-selected="false">Systeemgegevens</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contactpersoongegevens</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="active-tasks-tab" data-toggle="tab" href="#activeTasks" role="tab" aria-controls="activeTasks" aria-selected="false">Actieve taken</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="done-tasks-tab" data-toggle="tab" href="#doneTasks" role="tab" aria-controls="doneTasks" aria-selected="false">Afgehandelde taken</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Documenten</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="s4configurator-tab" data-toggle="tab" href="#s4configurator" role="tab" aria-controls="s4configurator" aria-selected="false">S4Configurator</a>
                            </li>
                        </ul>

                        <!-- Tabs content -->
                        <div class="tab-content mt-3" id="myTabContent">

                            <!-- Bedrijfsgegevens tab -->
                            <div class="tab-pane fade active show" id="bedrijf" role="tabpanel" aria-labelledby="bedrijf-tab">

                                <div class="row">

                        
                                    <div class="col-sm-6 mt-3 centered">

                                        <!-- Organisatieadres -->
                                        <label class="noselect" for="oa">Adres:&nbsp;</label><br>
                                        <span class="text-muted" id="oa"><?= $this->getOrgAddress() ?>&nbsp;<?= $this->getOrgHouseNumber() ?><br>
                                            <?= $this->getOrgPostalcode() ?><br>
                                            <?= $this->getOrgCity() ?>,&nbsp;<?= $this->getOrgCountry()?>

                                            <span>
                                                <br><br>

                                                <!-- Organisatie mail -->
                                                <label class="noselect" for="oe">Emailadres:&nbsp;</label>
                                                <span class="text-muted" id="oe"><?= $this->getOrgEmail()?></span><br>

                                                <!-- Organisatie tel -->
                                                <label class="noselect" for="ot">Telefoon:&nbsp;</label>
                                                <span class="text-muted" id="ot"><?= $this->getOrgPhone()?></span><br>

                                    </div>

                                    <div class="col-sm-6 mt-3 centered">

                                        <!-- Kredietverzekeraar -->
                                        <label class="noselect" for="kv">Kreditverzekeraar:&nbsp;</label>
                                        <span class="text-muted" style="" id="kv" style=""><?= $this->getOrgIns() ?></span><br>
                                        <!-- Informatiebureau -->
                                        <label class="noselect" for="ib">Informatiebureau:&nbsp;</label>
                                        <span class="text-muted" id="ib"><?= $this->getOrgInfDesk() ?></span><br>
                                        <!-- Incassopartner -->
                                        <label class="noselect" for="ip">Incassopartner:&nbsp;</label>
                                        <span class="text-muted" id="ip"><?= $this->getOrgIncassopartner() ?></span><br>
                                        <!-- Relatiebeheerder -->
                                        <label class="noselect" for="rb">Relatiebeheerder:&nbsp;</label>
                                        <span class="text-muted" id="rb"><?= $this->getOrgRelName() ?></span><br>
                                        <!-- Aangemaaktop -->
                                        <label class="noselect" for="ad">Aangemaakt op:&nbsp;</label>
                                        <span class="text-muted" id="ad"><?= $this->getOrgCreatedOn() ?></span><br>
                                        <!-- Laatst gewijzigd op -->
                                        <label class="noselect" for="ad">Laatst gewijzigd op:&nbsp;</label>
                                        <span class="text-muted" id="ad"><?= $this->getOrgUpdatedOn() ?></span><br>

                                    </div>
                                </div>
                            </div>

                            <!-- Systeemgegevens tab -->
                            <div class="tab-pane fade" id="systeem" role="tabpanel" aria-labelledby="systeem-tab">

                                <!-- ERP Systeem -->
                                <label class="noselect" for="sys_erp">ERP Systeem:&nbsp;</label>
                                <span class="text-muted" id="sys_erp"><?= $this->getOrgERP() ?></span><br>

                                <!-- CMS systeem -->
                                <label class="noselect" for="sys_cms">CMS Systeem:&nbsp;</label>
                                <span class="text-muted" id="sys_cms"><?= $this->getOrgCMS() ?></span><br>

                                <!-- CMS systeem -->
                                <label class="noselect" for="sys_ins">Kredietverzekeraar:&nbsp;</label>
                                <span class="text-muted" id="sys_ins"></span><br>

                                <!-- S4 Product -->
                                <label class="noselect" for="sys_s4products">S4 Product:&nbsp;</label>
                                <span class="text-muted" id="sys_s4products"><?= $this->getOrgS4ProdName() ?></span><br>

                            </div>

                            <!-- Contactpersoongegevens tab -->
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

                                <div class="row">

                                    <!-- Left div -->
                                    <div class="col-sm-4 mt-3 centered">

                                        <!-- Contact name -->
                                        <label class="noselect" for="cn">Naam:&nbsp;</label>
                                        <span class="text-muted" id="cn"><?= $this->getConFirstname() ?>&nbsp;<?= $this->getConLastname() ?></span><br>
                                        <!-- Contact function -->
                                        <label class="noselect" for="cf">Functie:&nbsp;</label>
                                        <span class="text-muted" id="cf"><?= $this->getConFunction() ?></span><br>
                                        <!-- Contact sex -->
                                        <label class="noselect" for="cs">Geslacht:&nbsp;</label>
                                        <span class="text-muted" id="cs"><?= ucfirst($this->getConSex()) ?></span><br>
                                    </div>

                                    <!-- Mid Div -->
                                    <div class="col-sm-4 mt-3 centered">
                                        <!-- Contact email -->
                                        <label class="noselect" for="ce">Emailadres:&nbsp;</label>
                                        <span class="text-muted" id="ce"><?= $this->getConEmail() ?></span><br>
                                        <!-- Contact landline -->
                                        <label class="noselect" for="cl">Tel vast:&nbsp;</label>
                                        <span class="text-muted" id="cl"><?= $this->getConLandline() ?></span><br>
                                        <!-- Contact mobile -->
                                        <label class="noselect" for="cm">Tel mobiel:&nbsp;</label>
                                        <span class="text-muted" id="cm"><?= $this->getConMobile() ?></span><br>
                                    </div>

                                    <!-- Right div -->
                                    <div class="col-sm-4 mt-3 centered">
                                        <!-- Contact email -->
                                        <label class="noselect" for="cc">Aangemaakt op:&nbsp;</label>
                                        <span class="text-muted" id="cc"><?= $this->getConCreatedOn() ?></span><br>
                                        <!-- Contact landline -->
                                        <label class="noselect" for="cu">Laatst gewijzigd op:&nbsp;</label>
                                        <span class="text-muted" id="cu"><?= $this->getConUpdatedOn() ?></span><br>
                                    </div>

                                </div>
                            </div>

                            <!-- Actieve taken tab -->
                            <div class="tab-pane fade" id="activeTasks" role="tabpanel" aria-labelledby="active-tasks-tab">
                                            <div class="single-table">
                                                <div class="table-responsive">
                                                    <table id="tasks" class="table table-hover progress-table text-center">
                                                        <!-- Table headers -->
                                                        <thead class="text-uppercase">
                                                            <tr>
                                                                <th scope="col">Omschrijving</th>
                                                                <th scope="col">Actiehouder</th>
                                                                <th scope="col">Opvolgactie</th>
                                                                <th scope="col">Opvolgdatum</th>
                                                                <th scope="col">Contactpersoon</th>
                                                                <th scope="col">Tel</th>
                                                                <th scope="col">Acties</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?=$this->renderFollowUpTakenTable()?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                
                            </div>

                            <!-- Afgehandelde taken tab -->
                            <div class="tab-pane fade" id="doneTasks" role="tabpanel" aria-labelledby="done-tasks-tab">
                            <div class="single-table">
                                    <div class="table-responsive">
                                        <table id="tasks" class="table table-hover progress-table text-center">
                                            <!-- Table headers -->
                                            <thead class="text-uppercase">
                                                <tr>
                                                    <th scope="col">Taak</th>
                                                    <th scope="col">Actiehouder</th>
                                                    <th scope="col">Opvolgactie</th>
                                                    <th scope="col">Opvolgdatum</th>
                                                    <th scope="col">Acties</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php //echo $this->renderSettledFollowUpTakenTable()?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- S4Configurator tab -->
                            <div class="tab-pane fade centered" id="s4configurator" role="tabpanel" aria-labelledby="s4configurator-tab">
                                <br>
                                <h6 class="centered">Klik <a href="https://mijnprojecten.ml/s4-configurator/login.php">hier</a> om naar de configurator te gaan</h6>
                                <br>
                            </div>

                            <!-- Documenten tab -->
                            <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                                Documenten hier
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </form>
        <!-- End form -->
        </div>

    <?php }

        /* Render Support organisation view (inside company view) */
        public function renderSupportOrganisationView()
        {
            echo "Support View";
        }

        /* Render Administratie view (inside company view) */
        public function renderAdministratieOrganisationView()
        {
            echo "Administration View";
        }


        /* Create new user (login) and save into database */
        public function createUser($username, $password, $rol, $email, $isAdmin, $isRelManager)
        {

            $this->userName         = $username;
            $this->userPass         = $password;
            $this->userEmail        = $email;
            $this->userRol          = $rol;
            $this->userIsAdmin      = $isAdmin;
            $this->userIsRelManager = $isRelManager;

            require_once('dbconfig.php');
            $conn = DatabaseConnection::getConnection();
            $q    = "INSERT INTO gebruikers (gebruikersnaam, email, wachtwoord, isAdmin, isRelatieBeheerder, rol) VALUES('$this->userName', '$this->userEmail', '$this->userPass', '$this->userIsAdmin', '$this->userIsRelManager', '$this->userRol')";
            $stmt = $conn->prepare($q);
            if ($stmt->execute()) {
                header("Location: beheer.php?success=newUser");
            } else {
                header("Location: beheer.php?error=newUser");
            }
        }

        /* Update/edit user details (login) and save into database */
        public function editUser($userid, $username, $useremail, $userpass, $userrol, $userisadmin, $userisrelmanager, $userisactive)
        {
            $this->userID = $userid;
            $this->userName = $username;
            $this->userEmail = $useremail;
            $this->userPass = $userpass;
            $this->userRol = $userrol;
            $this->userIsAdmin = $userisadmin;
            $this->userIsRelManager = $userisrelmanager;
            $this->userIsActive = $userisactive;

            require_once "dbconfig.php";
            $conn = DatabaseConnection::getConnection();
            $q = "UPDATE gebruikers SET `gebruikersnaam`='$this->userName', `email`='$this->userEmail', `wachtwoord`='$this->userPass', `isAdmin`='$this->userIsAdmin', `isActive`='$this->userIsActive', `isRelatiebeheerder`='$this->userIsRelManager', `rol`='$this->userRol' WHERE id='$this->userID'";
            $stmt = $conn->prepare($q);
            if ($stmt->execute()) {
                header("Location: beheer.php?userEditSuccess");
            } else {
                header("Location: beheer.php?userEditError");
            }
        }

        /* Update/edit organisation details and save into database */
        public function editOrganisation(
            $orgid,
            $orgnumber,
            $orgname,
            $orgaddress,
            $orghousenr,
            $orgpostal,
            $orgcity,
            $orgcountry,
            $orgtel,
            $orgemail,
            $orgstatus,
            $orgerp,
            $orgcms,
            $orgins,
            $orginf,
            $orgincasso,
            $orgs4prod,
            $orgrel,
            $orgnotes
        ) {
            $this->orgId             = $orgid;
            $this->orgNumber         = $orgnumber;
            $this->orgName           = $orgname;
            $this->orgAddress        = $orgaddress;
            $this->orgHouseNumber    = $orghousenr;
            $this->orgPostalCode     = $orgpostal;
            $this->orgCity           = $orgcity;
            $this->orgCountry        = $orgcountry;
            $this->orgPhone          = $orgtel;
            $this->orgEmail          = $orgemail;
            $this->orgStatus         = $orgstatus;
            $this->orgERP            = $orgerp;
            $this->orgCMS            = $orgcms;
            $this->orgIns            = $orgins;
            $this->orgInfDesk        = $orginf;
            $this->orgIncassoPartner = $orgincasso;
            $this->orgS4Prod         = $orgs4prod;
            $this->orgRel            = $orgrel;
            $this->orgNotes          = $orgnotes;

            require_once "dbconfig.php";
            $conn = DatabaseConnection::getConnection();
            $q = "UPDATE organisatie SET 
            `organisatienummer`  = '$this->orgNumber',
            `organisatienaam`    = '$this->orgName',
            `adres`              = '$this->orgAddress',
            `huisnummer`         = '$this->orgHouseNumber',
            `postcode`           = '$this->orgPostalCode',
            `plaats`             = '$this->orgCity',
            `land`               = '$this->orgCountry',
            `telefoon`           = '$this->orgPhone',
            `email`              = '$this->orgEmail',
            `status`             = '$this->orgStatus',
            `notities`           = '$this->orgNotes',
            `erp`                = '$this->orgERP',
            `cms`                = '$this->orgCMS',
            `kredietverzekeraar` = '$this->orgIns',
            `informatiebureau`   = '$this->orgInfDesk',
            `incassopartner`     = '$this->orgIncassoPartner',
            `s4_product`         = '$this->orgS4Prod',
            `relatiebeheerder`   = '$this->orgRel' WHERE `id`='$this->orgId'";

            $stmt = $conn->prepare($q);
            if ($stmt->execute()) {
                header("Location: organisaties.php?orgEditSuccess");
            } else {
                header("Location: organisaties.php?orgEditError");
            }
        }

        /* Create notification for creating new organisations (Success & Error) */
        // public function createNotification()
        // {
        //     /* Check if new organisation has been made or not */
        //     if (isset($_GET['success'])) {

        //         /* Get the notification type */
        //         $this->notifyType = $_GET['success'];

        //         /* Notification on new organisation */
        //         if ($this->notifyType === 'newOrganisation') {
        //             $this->notifyMsg = "Invoeren van nieuwe organisatie gelukt!";
        //             $this->notify    = "<div class='stickyNotification notifySuccess noselect'><h5>" . $this->notifyMsg . "</h5></div>";
        //         }
        //         /* Notification on new contact */
        //         if ($this->notifyType === 'newContact') {
        //             $this->notifyMsg = "Invoeren van nieuwe contactpersoon gelukt!";
        //             $this->notify    = "<div class='stickyNotification notifySuccess noselect'><h5>" . $this->notifyMsg . "</h5></div>";
        //         }
        //         /* Notification on new contact */
        //         if ($this->notifyType === 'deleted') {
        //             $this->notifyMsg = "Bestand verwijderd!";
        //             $this->notify    = "<div class='stickyNotification notifySuccess noselect'><h5>" . $this->notifyMsg . "</h5></div>";
        //         }
        //     } elseif (isset($_GET['error'])) {

        //         /* Get the notification type */
        //         $this->notifyType = $_GET['error'];

        //         /* Notification on new organisation (error) */
        //         if ($this->notifyType === 'newOrganisation') {
        //             $this->notifyMsg = "Invoeren van nieuwe organisatie mislukt...";
        //             $this->notify    = "<div class='stickyNotification notifyError noselect'><h5>" . $this->notifyMsg . "</h5></div>";
        //         }
        //         /* Notification on new contact (error) */
        //         if ($this->notifyType === 'newContact') {
        //             $this->notifyMsg = "Invoeren van nieuwe contactpersoon mislukt...";
        //             $this->notify    = "<div class='stickyNotification notifyError noselect'><h5>" . $this->notifyMsg . "</h5></div>";
        //         }
        //         /* Notification on new contact */
        //         if ($this->notifyType === 'notDeleted') {
        //             $this->notifyMsg = "Bestand verwijderd!";
        //             $this->notify    = "<div class='stickyNotification notifySuccess noselect'><h5>" . $this->notifyMsg . "</h5></div>";
        //         }
        //     } else {
        //         $this->notify = null;
        //     }
        //     return $this->notify;
        // }

        public function getNotify()
        {
            return $this->notify;
        }

        public function renderMenu()
        {
            ?>
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="index.php"><img src="assets/images/icon/logo.png" alt="logo"></a>
                </div>
            </div>

            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">

                            <li class="">
                                <a href="index.php"><i class="ti-dashboard"></i><span>dashboard</span></a>
                            </li>

                            <li class="">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-organisations"></i><span>Organisatie</span></a>
                                <ul class="collapse">
                                    <li class=""><a href="organisaties.php"><i class="ti-organisations"></i>&nbsp; Organisaties</a></li>
                                    <li class=""><a href="contactpersonen.php"><i class="ti-contacts"></i>&nbsp; Contactpersonen</a></li>
                                    <li class=""><a href="bijlagen.php"><i class="ti-file"></i>&nbsp; Bijlagen</a></li>
                                    <!-- <li><a href="organisaties.php"><i class="ti-organisations"></i>&nbsp;Alle organisaties</a></li>
                                    <li><a href="contactpersonen.php"><i class="ti-contacts"></i>&nbsp;Alle contactpersonen</a></li> -->
                                </ul>
                            </li>

                            <li class="">
                                <a href="taken.php" aria-expanded="true"><i class="ti-actions"></i><span>Taken</span></a>
                            </li>

                            <li class="">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-view-list"></i><span>Overzichten</span></a>
                                <ul class="collapse">
                                    <li class=""><a href="#"><i class="ti-contracts">&nbsp;</i>Contracten</a></li>
                                    <li><a href="#"><i class="ti-import">&nbsp;</i>Imports</a></li>
                                    <li><a href="#"><i class="ti-ticket">&nbsp;</i>Tickets</a></li>
                                </ul>
                            </li>

                            <li class="">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-reports"></i><span>Rapportages</span></a>
                                <ul class="collapse">
                                    <li class=""><a href="index.html">Test 1</a></li>
                                    <li><a href="#">Test 2</a></li>
                                    <li><a href="#">Test 3</a></li>
                                </ul>
                            </li>

                            <!-- Beheer is alleen zichtbaar voor admins -->
                            <?php if ($_SESSION['isAdmin'] === "1") { ?>
                                <li class="">
                                    <a href="beheer.php"><i class="ti-settings"></i><span>Beheer</span></a>
                                </li>
                            <?php } ?>

                            <br>
                            <span style="display:block;border-bottom:1px solid #08475b;"></span>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->
    <?php
        }

        public function renderFooter()
        { ?>
        <!-- footer area start-->
        <footer>
            <div class="footer-area">
                <p>© S4 Portal - Alle rechten voorbehouden.</p>
            </div>
        </footer>
        <!-- footer area end-->
    <?php
        }

        public function renderPageTitle($pageTitle)
        {

            $this->pageTitle = $pageTitle;
            $this->userRol = $_SESSION['userRol'];
            $profileIcon = "";
            $profileTitle = "";

            if ($this->userRol === "1") {
                $profileIcon = "assets/images/profile-icons/profile-administratie.png";
                $profileTitle = "Administratie";
            }
            if ($this->userRol === "2") {
                $profileIcon = "assets/images/profile-icons/profile-sales.png";
                $profileTitle = "Sales";
            }
            if ($this->userRol === "3") {
                $profileIcon = "assets/images/profile-icons/profile-ontwikkeling.png";
                $profileTitle = "Ontwikkeling";
            }
            if ($this->userRol === "4") {
                $profileIcon = "assets/images/profile-icons/profile-support.png";
                $profileTitle = "Support";
            }

            ?>

        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">

                    <!-- Begin breadcrumbs -->
                    <?php $this->renderBreadcrumbs() ?>
                    <!-- End breadcrumbs -->

                </div>

                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <img title="<?= $profileTitle ?>" class="avatar user-thumb" src="<?= $profileIcon ?>" alt="avatar">
                        <h4 class="user-name dropdown-toggle" data-toggle="dropdown"><?= $_SESSION['userName']; ?><i class="fa fa-angle-down"></i></h4>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Instellingen</a>
                            <a class="dropdown-item" href="controller.php?logout">Uitloggen</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    <?php
        }

        public function renderPreloader()
        { ?>
        <div id="preloader">
            <div class="loader"></div>
        </div>
    <?php }

        public function renderNavAndSearch()
        { ?>

        <!-- Menu slide button -->
        <div class="col-md-6 col-sm-8 clearfix">
            <div class="nav-btn pull-left">
                <span></span>
                <span></span>
                <span></span>
            </div>


            <!-- Header Search Box: Find organisations -->
            <div class="search-box pull-left">
                <input type="text" name="search" id="search-box" placeholder="Zoeken..." required="" autocomplete="off">
                <i class="ti-search"></i>
                <div id="result-box"></div>
            </div>

        </div>
    <?php }

        public function renderProfileAndNotifications()
        { ?>
        <div class="col-md-6 col-sm-4 clearfix">
            <ul class="notification-area pull-right">
                <li id="full-view"><i class="ti-fullscreen"></i></li>
                <li id="full-view-exit"><i class="ti-zoom-out"></i></li>

                <li class="dropdown">
                    <i class="ti-bell dropdown-toggle" data-toggle="dropdown">
                        <span class="n-count notify-count">
                        </span>
                    </i>
                    <div class="dropdown-menu bell-notify-box notify-box">
                        <span class="notify-title">Je hebt <span class="n-count notify-count"></span> nieuwe notificaties<br><br>
                            <a class="mark-as-read" href="javascript:void(0)">Alles als gelezen markeren</a></span>
                        <div class="n-list notify-list"></div>
                    </div>
                </li>

                <li class="dropdown">
                    <i class="fa fa-envelope-o dropdown-toggle" data-toggle="dropdown"><span>0</span></i>
                    <div class="dropdown-menu notify-box nt-enveloper-box">
                        <span class="notify-title">Je hebt 0 nieuwe berichten<br>
                            <a href="#">Open alle berichten</a></span>
                        <div class="notify-list">

                            <!-- <a href="#" class="notify-item">
                        <div class="notify-thumb">
                            <img src="assets/images/author/placeholder.jpg" alt="image">
                        </div>
                        <div class="notify-text">
                            <p>Marcel Blanken</p>
                            <span class="msg">Contract is getekend. Even contact opnemen</span>
                            <span>3:15 PM</span>
                        </div>
                    </a> -->

                        </div>
                    </div>
                </li>
                <li class="settings-btn">
                    <i title="Instellingen" class="ti-settings"></i>
                </li>
            </ul>
        </div>
    <?php }

        public function renderBreadcrumbs()
        {
            ?>
        <div class="breadcrumbs-area clearfix">
            <h4 class="page-title pull-left noselect">
                <!-- <img title="S4Dunning" style="position:relative;top:-2px;" width="29" src="assets/images/logo/S4D.png"> -->
                <?= $this->pageTitle ?>
            </h4>

            <!-- <ul class="breadcrumbs pull-left">
                            <li><a href="index.php">Home</a></li>
                            <li><span></span></li>
                        </ul> -->
        </div>
<?php }
}
