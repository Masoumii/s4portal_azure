<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/* Start session if not already started */
 if(!isset($_SESSION)){session_start();}
 
/* Class */
class Login
{
    /* Private variables */
    private $userId       = null;
    private $userIP       = null;
    private $userName     = null;
    private $userEmail    = null;
    private $userPass     = null;
    private $userRol      = null;
    private $dbPass       = null;
    private $isAdmin      = false;
    private $isRelManager = false;
    private $isActive     = false;
    private $login        = false;
    
    /* Constructor */
    public function __construct(){}
    
    /* Login method */
    public function logUserIn($email, $pass){
        
         $_SESSION['loggedIn']     = false;   // Set session
                  $this->email     = $email;  // Email from user input
                  $this->userPass  = $pass;   // Password from user input
        
        include("dbconfig.php");
        $conn = DatabaseConnection::getConnection();
        $q    = "SELECT * FROM gebruikers WHERE email = '$this->email' AND isActive=1";
        $stmt = $conn->prepare($q);
        $stmt->execute();
        
        while ($row = $stmt->fetch()){
            $this->userId       = $row['id'];
            $this->userName     = $row['gebruikersnaam'];
            $this->userEmail    = $row['email'];
            $this->isAdmin      = $row['isAdmin'];
            $this->isActive     = $row['isActive'];
            $this->dbPass       = $row['wachtwoord'];
            $this->userRol      = $row['rol'];
            $this->isRelManager = $row['isRelatiebeheerder'];
            $this->userIP       = $_SERVER['REMOTE_ADDR'];
        } 
        
            /* Password-check: Compare user pass with DB pass */
            if ($this->userPass === $this->dbPass){
                
                $_SESSION['loggedIn']   = true;              // Set session
                $_SESSION['user_id']    = $this->userId;     // Save ID in session for later use
                $_SESSION['userName']   = $this->userName;   // Save the username in session 
                $_SESSION['user_email'] = $this->userEmail;  // Save the username in Session to show to user
                $_SESSION['isAdmin']    = $this->isAdmin;    // Save isAdmin value in session
                $_SESSION['isActive']   = $this->isActive;   // Checks if user is activated and can log in or not
                $_SESSION['userRol']    = $this->userRol;    // Keeps track of the user's rol to load fitting Profile icons
                
                /* Update last login datetime in database */
                $conn = DatabaseConnection::getConnection(); 
                $q    = "UPDATE gebruikers SET laatsteLogin = now(), laatsteIP = '$this->userIP' WHERE id = ".$this->userId;
                $stmt = $conn->prepare($q);
                
                /* login success */
                if($stmt->execute()){

                    $this->login = true;
                    // Code hier als inloggen gelukt is
                }         
            else{
                $_SESSION['notifyMsg'] = "<span class='errorMsg'>Combinatie email/wachtwoord niet herkend&nbsp;&nbsp;<img width='35' src='img/error.png'></span>";
            } 
           }
    }
    
    /* Get login value: logged in or not */
    public function getLogin()
    {
        return $this->login;
    }
    
    /* Get USER ID */
    public function getId()
    {
        return $this->userId;
    }
    
    /* Log out method */
    public function logUserOut()
    {
        session_start();   // Start session
        session_unset();   // Unset all session variables
        session_destroy(); // Destory session 
        header("location: login.php");
    }

}