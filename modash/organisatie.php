<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}

/* If user is not logged in, redirect him back to login page */
if ($_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

/* Initialise the class and assign variable to use it below */
require "classes/s4portal.class.php";
$S4Portal = new S4Portal();
$S4Portal->viewOrganisation($_GET['id']);
?>

<!doctype html>
<!-- no-js until js is attached to it -->
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <!-- IE -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Tab name -->
    <title>S4Portal</title>
    <!-- Top headers -->
    <?php require "headers.php"; ?>
</head>

<body>
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->

    <!-- page container area start -->
    <div class="page-container">

        <!-- sidebar menu area start -->
        <?php $S4Portal->renderMenu(); ?>
        <!-- sidebar menu area end -->

        <!-- main content area start -->
        <div class="main-content">


            <!-- header area start -->
            <div class="header-area">

                <div class="row align-items-center">

                    <!-- nav and search button start -->
                    <?php $S4Portal->renderNavAndSearch(); ?>
                    <!-- nav and search button end -->

                    <!-- profile info & task notification start -->
                    <?php $S4Portal->renderProfileAndNotifications(); ?>
                    <!-- profile info & task notification end -->

                </div>
            </div>
            <!-- header area end -->

            <!-- page title area start -->
            <?php

            if ($S4Portal->getOrgStatus() == 4) {
                $customerImg = "<img class='titleImg' width='25' src='assets/images/icon/klant.png'>";
            } else if ($S4Portal->getOrgStatus() == 3) {
                $customerImg = "<img class='titleImg' width='25' src='assets/images/icon/prospect.png'>";
            } else if ($S4Portal->getOrgStatus() == 2) {
                $customerImg = "<img class='titleImg' width='25' src='assets/images/icon/suspect.png'>";
            } else {
                $customerImg = "<img class='titleImg' width='25' src='assets/images/icon/inactieve-klant.png'>";
            }

            ?>
            <?php $S4Portal->renderPageTitle($customerImg . "&nbsp;&nbsp;" . $S4Portal->getOrgName()) ?>
            <!-- page title area end -->

            <div class="main-content-inner">

                <!-- Accordeon Sales -->
                <div class="row-fluid">
                    <br>
                    <!-- Accordeon menu -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- Start accordion menu -->
                                <div id="accordion2" class="according accordion-s2">
                                    <div id="accordion2" class="according accordion-s2">

                                        <div class="card">
                                            <div class="card-header">
                                                <a style="color:#08475b" class="card-link" data-toggle="collapse" href="#accordion21"><i class="ti-credit-card">&nbsp;</i>Sales</a>
                                            </div>
                                            <div id="accordion21" class="collapse show" data-parent="#accordion2">
                                                <div class="card-body">
                                                    <?= $S4Portal->renderSalesOrganisationView() ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-header">
                                                <a style="color:#08475b" class="collapsed card-link" data-toggle="collapse" href="#accordion22"><i class="ti-support">&nbsp;</i>Support</a>
                                            </div>
                                            <div id="accordion22" class="collapse" data-parent="#accordion2">
                                                <div class="card-body">
                                                    <?= $S4Portal->renderSupportOrganisationView() ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-header">
                                                <a style="color:#08475b" class="collapsed card-link" data-toggle="collapse" href="#accordion23"><i class="ti-agenda"></i>&nbsp;Administratie</a>
                                            </div>
                                            <div id="accordion23" class="collapse" data-parent="#accordion2">
                                                <div class="card-body">
                                                    <?= $S4Portal->renderAdministratieOrganisationView() ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main content area end -->

        <!-- footer area start-->
        <?php $S4Portal->renderFooter(); ?>
        <!-- footer area end-->
    </div>

    <!-- Bottom headers required on all pages -->
    <?php require "headersBottom.php"; ?>
</body>

</html>