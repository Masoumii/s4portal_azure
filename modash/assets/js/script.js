$(function(){


    /* Disable upload if no file chosen on page load */
    $('#hide-btn').attr('disabled',true);

      // Table Search/filter Setup - add a text input to each footer cell
      $('#organisations thead th, #contacts thead th, #tasks thead th').each( function () {
        // Defines the column title to exclude
        var colTitle = $(this).text();
        /* Exclude search on column 'acties' */
        if(colTitle !== 'Acties'){
            $(this).html( '<input type="text" class="form-control centered" placeholder='+colTitle+' />' );
        }
        
    } );

    /* Initialise the dataTable */
   $('#organisations, #contacts, #tasks').DataTable({

        //"columns": [{ "type": "file-size", "targets": 2 }],
         "paging"        : true,      // Enable or disable paging
        //  "scrollY"       : "250px",   // Height scroll area
         "scrollCollapse": false,     // Enable scroll collapse

        /* Adjust column width automatically */
        // Turn this off if youre no interested in auto width
        "autoWidth": true,

        /* Language translations */
        "language": {
            "lengthMenu"  : "_MENU_ resultaten per pagina",
            "zeroRecords" : "Geen data gevonden",
            "info"        : "_PAGE_ van _PAGES_",
            "infoEmpty"   : "Geen data beschikbaar",
            "search"      : "Zoeken:",
            "infoFiltered": "(Gefiltered uit totaal _MAX_ resultaten)",

            /* Pagination translations */  
            "paginate": {
                "first"   : "Eerste",
                "last"    : "Laatste",
                "next"    : "Volgende",
                "previous": "Vorige"
            },
        }
    }).columns().every(function(){
        var that = this;
        // Column search/filtering
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that.search( this.value ).draw();
            }
        } );
    } );


    /* Edit user on click: Open modal */
    $(".edit-user").on("click", function(){

        /* Get user details to be used in the modal for editting */
        let userId        = $(this).closest("li").data("userid");
        let username      = $(this).closest("li").data("username");
        let email         = $(this).closest("li").data("email");
        let pass          = $(this).closest("li").data("pass");
        let rol           = $(this).closest("li").data("rol");
        let isActive      = $(this).closest("li").data("isactive");
        let s4portalAdmin = $(this).closest("li").data("isadmin");
        let relManager    = $(this).closest("li").data("isrelmanager");
        

        /* On modal load, insert user details values into inputs */
       $('#exampleModalLong').on('show.bs.modal', function (e) {

        /* Set the form action depending on user ID */
        $('#editUserDetails').attr('action', 'controller.php?editUser='+userId);

            /* Push user values to inputs in the modal */
            $(".editUserId").val(userId);
            $(".editUsername").val(username);
            $(".editEmail").val(email);
            $(".editPassword").val(pass);

            /* Rollen: Select the right option depending on selected user / value from DB */
            if(rol ===rol){
                $('select[name="userRol"]').find('option[value="'+rol+'"]').prop("selected",true);
            }
             /* S4Portal Admin : Select the right option depending on selected user  / value from DB */
             if(s4portalAdmin === s4portalAdmin){
                $('select[name="isAdmin"]').find('option[value="'+s4portalAdmin+'"]').prop("selected",true);
            }
             /* Relatiebeheerder: Select the right option depending on selected user  / value from DB */
             if(relManager === relManager){
                $('select[name="isRelManager"]').find('option[value="'+relManager+'"]').prop("selected",true);
            }
            /* Is Active: Select the right option depending on selected user  / value from DB */
            if(isActive === isActive){
                $('select[name="isActive"]').find('option[value="'+isActive+'"]').prop("selected",true);
            }

        })
    })

    $(".complete-task").on("click", function(){
        alert( "test")
    })

        /* Assign task on click: Open modal */
        $(".assign-task").on("click", function(){

            /* Get task details to be used in the modal for editting */
            let taskId             = $(this).closest("li").data("task-id")
            let originalTaskId     = $(this).closest("li").data("original-task-id");
            let taskOrgId          = $(this).closest("li").data("task-org-id");
            let taskCreator        = $(this).closest("li").data("task-creator");
            let taskOwner          = $(this).closest("li").data("task-followup-owner");
            let taskCreationDate   = $(this).closest("li").data("task-creationdate");
            let taskFollowUpAction = $(this).closest("li").data("task");
            let taskActionHolder   = $(this).closest("li").data("task-action-holder");
            let taskFollowUpDate   = $(this).closest("li").data("task");

            console.log(taskId);
            console.log(originalTaskId);
            
            /* On modal load, insert user details values into inputs */
           $('#tasksModal').on('show.bs.modal', function (e){
                /* Push user values to inputs in the modal */
                $('.taskId').val(taskId); // Task ID (ID of original task that is being followed up)
                $('.originalTaskId').val(originalTaskId); // Task ID (ID of original task that is being followed up)
                $('.taskOrgId').val(taskOrgId); // The ID of the original task
                $('.taskCreator').val(taskCreator); // Creator of the task (not to be confused with owner)
                $('.taskOwner').val(taskOwner); // Current "owner" of the task
                $('.taskCreationDate').val(taskCreationDate); // Task creation data
                $('.taskFollowUpAction').val(taskFollowUpAction); // Task follow-up action
                $('.taskFollowUpHolder').val(taskActionHolder); // Task Action holder
                $('.taskFollowUpDate').val(taskFollowUpDate); // Task follow up date
            })
        })


        /* Open up TASK HISTORY */
        $(".task-history").on("click", function(){

            let historyId = $(this).data('history-id');

            /* On modal load, show task history */
           $('#historyModal').on('show.bs.modal', function (e){

            $.post( "controller.php", { historyId:historyId }, function( data ) { 
                //add result to a div and create a modal/dialog popup similar to alert()
                $(".task-history-body").html(data);
          });
               
                
            })  
        })

    

         /* Edit contact details on click: Open modal */
         $(".edit-contact").on("click", function(){

            /* Get task details to be used in the modal for editting */
            let conID        = $(this).closest("i").data("conid");
            let conOrg       = $(this).closest("i").data("conorg");
            let conOrgID     = $(this).closest("i").data("conorgid");
            let conSex       = $(this).closest("i").data("consex");
            let conFirstName = $(this).closest("i").data("confirstname");
            let conLastName  = $(this).closest("i").data("conlastname");
            let conFunction  = $(this).closest("i").data("confunction");
            let conEmail     = $(this).closest("i").data("conemail");
            let conLandline  = $(this).closest("i").data("conlandline");
            let conMobile    = $(this).closest("i").data("conmobile");
            let conType      = $(this).closest("i").data("contype");
            let conTypeID    = $(this).closest("i").data("contypeid");
            
            /* On modal load, insert user details values into inputs */
           $('#contactModal').on('show.bs.modal', function (e) {

            /* Set the form action depending on user ID */
            $('#editContactDetails').attr('action', 'controller.php?editContact='+conID);
    
                /* Push user values to inputs in the modal */
                $(".editConID").val(conID);
                 $(".editConOrg").val(conOrg);
                 $(".editConSex").val(conSex);
                 $(".editConFirstName").val(conFirstName);
                 $(".editConLastName").val(conLastName);
                 $(".editConFunction").val(conFunction);
                 $(".editConEmail").val(conEmail);
                 $(".editConLandline").val(conLandline);
                 $(".editConMobile").val(conMobile);
                 $(".editConType").val(conType);

                 /* Select the right organisation in the select box based on value from DB */
                 if(conOrgID===conOrgID){
                    $('select[name="conOrg"]').find('option[value="'+conOrgID+'"]').prop("selected",true);
                 }
                 if(conSex===conSex){
                    $('select[name="conSex"]').find('option[value="'+conSex+'"]').prop("selected",true);
                 }
                 if(conType===conType){
                    $('select[name="conType"]').find('option[value="'+conType+'"]').prop("selected",true);
                 }
    
            })
    
        })

    /* Edit organisation on click */
    $(".edit-org").on("click", function(){

        /* Get user details to be used in the modal for editting */
        let orgId        = $(this).closest("li").data("orgid");
        let orgNum       = $(this).closest("li").data("orgnum");
        let orgName      = $(this).closest("li").data("orgname");
        let orgAddress   = $(this).closest("li").data("orgaddress");
        let orgHousenr   = $(this).closest("li").data("orghousenr");
        let orgPostal    = $(this).closest("li").data("orgpostal");
        let orgCity      = $(this).closest("li").data("orgcity");
        let orgCountry   = $(this).closest("li").data("orgcountry");
        let orgPhone     = $(this).closest("li").data("orgphone");
        let orgEmail     = $(this).closest("li").data("orgemail");
        let orgStatus    = $(this).closest("li").data("orgstatus");
        let orgNotes     = $(this).closest("li").data("orgnotes");
        let orgErp       = $(this).closest("li").data("orgerp");
        let orgCMS       = $(this).closest("li").data("orgcms");
        let orgIns       = $(this).closest("li").data("orgins");
        let orgInf       = $(this).closest("li").data("orginf");
        let orgIncasso   = $(this).closest("li").data("orgincasso");
        let orgS4prod    = $(this).closest("li").data("orgs4prod");
        let orgRel       = $(this).closest("li").data("orgrel");
        let relOptionName = $(".reloption").data("optionname");
        let relOptionId   = $(".reloption").data("optionid");

                /* On modal load, insert user details values into inputs */
       $('#orgModal').on('show.bs.modal', function(e){

        /* Set the form action depending on user ID */
        $('#editOrgDetails').attr('action', 'controller.php?editOrg='+orgId);

            /* Push user values to inputs in the modal */
            $(".editOrgId").val(orgId);
            $(".editOrgNum").val(orgNum);
            $(".editOrgName").val(orgName);
            $(".editOrgAddress").val(orgAddress);
            $(".editOrgHousenr").val(orgHousenr);
            $(".editOrgPostal").val(orgPostal);
            $(".editOrgCity").val(orgCity);
            $(".editOrgCountry").val(orgCountry);
            $(".editOrgPhone").val(orgPhone);
            $(".editOrgEmail").val(orgEmail);
            $(".editOrgStatus").val(orgStatus);
            $(".editOrgNotes").val(orgNotes);
            $(".editOrgERP").val(orgErp);
            $(".editOrgCMS").val(orgCMS);
            $(".editOrgIns").val(orgIns);
            $(".editOrgInf").val(orgInf);
            $(".editOrgIncasso").val(orgIncasso);
            $(".editOrgRel").val(orgRel); // Relatiebeheerder ID

            /* S4 Product: Select the right option depending on selected organisation / value from DB */
            if(orgS4prod === orgS4prod){
                $('select[name="orgS4Prod"]').find('option[value="'+orgS4prod+'"]').prop("selected",true);
            }
            /* Status klant: Select the right option depending on selected organisation / value from DB */
            if(orgStatus ===orgStatus){
                $('select[name="orgStatus"]').find('option[value="'+orgStatus+'"]').prop("selected",true);
            }
            /* Relatiebeheerder: Select the right option depending on selected organisation / value from DB */
            if(relOptionId === relOptionName){
                $('select[name="orgStatus"]').find('option[value="'+relOptionId+'"]').prop("selected",true);
            }
            
        })
    })

    /* Delete file onclick on TRASH icon in table */
    $(".del-file").on("click", function () {
        let r = confirm("Bestand verwijderen?");
        if (r !== true) {
            return false;
        }
    })
    
    /* Hide notification bar on-click */
    $(".stickyNotification").on("click", function(){
        $(this).slideUp();
    });

    /* Slide up the notification bar */
    setTimeout(function(){
       $('.stickyNotification').slideUp();
        }, 3000);
        
        /* File upload check */
        $(':file').on('change',function(){

            var file = this.files[0];
            let fileName = file.name;
            let fileExt = fileName.split('.').pop();
            //let fileExt2 = file.type;

            /* If file name not empty */
            if(fileName !== ""){

                /* If accepted extension */
                if (fileExt == "pdf" || // PDF
                fileExt == "doc" || // DOC
                fileExt == "xls" || // XLS
                fileExt == "xlsx" || // XLSX
                fileExt == "" || // XLSX
                fileExt == "docx") { // DOCX

                $('#hide-btn').attr('disabled', false).html("<i class='ti-upload'></i>&nbsp;&nbsp;&nbsp;Nu uploaden");

                /* If chosen file is not allowed */
            } else{
                alert('Alleen DOC(X), XLS(X) & PDF bestanden zijn toegestaan.');
                $('#hide-btn').attr('disabled', true).html("Kies eerst een bestand");
                this.value = '';
                return false;
            }
            /* If there is no file chosen */
            }else{
                $('#hide-btn').attr('disabled', true).html("Kies eerst een bestand");
            }

        });

            /* Autocomplete header search function */
            $("#search-box").keyup(function(){

                /* If search box is has some value and not empty */
                if($(this).val()!==""){
                    $.ajax({
                        type: "POST",
                        url: "controller.php?search",
                        data:'keyword='+$(this).val(),
        
                        beforeSend: function(){
                            //$("#search-box").css("background","#FFF url(../images/LoaderIcon.gif) no-repeat 165px");
                        },
        
                        success: function(data){
                            $("#result-box").show();
                            $("#result-box").html(data);
                            $(".search-box input").css({
                                "border-bottom-left-radius": "0px",
                                "border-bottom-right-radius": "0px",
                                "border-top-left-radius":"20px",
                                "border-top-right-radius":"20px"
                            });
                        },
        
                        });
                /* If empty, hide suggestion box */
                 }else{
                     $("#result-box").hide();
                      $(".search-box input").css({
                                "border-radius":"20px"
                            })
                 }
               
            });

            /* If clicked outside searchbox or resultbox, hide search results */
            $(document).on('click', function(e) {
                if ( e.target.id != 'search-box' || e.target.id != 'result-box') {
                   $("#result-box").hide();
                   $(".search-box input").css({
                    "border-bottom-left-radius": "20px",
                    "border-bottom-right-radius": "20px",
                    "border-top-left-radius":"20px",
                    "border-top-right-radius":"20px"
                }).val("");
                }
            });

            /* Show/Hide and change disabled property based on selected value for follow-up */
            $('#taskFollowUp').change(function(){

                /* Get selected value */
                let selectVal = $(this).val();

                /* If value is zero (no followup) */
                if (selectVal === '0'){
                    $('.hidableOptions').css("visibility","hidden"); // Hide the inputs
                    $('#taskFollowUpAction, #taskFollowUpHolder, #taskFollowUpDate').prop('required', false).val(""); // Disable the inputs and remove required attribute
                }
                /* If value not zero (followup) */
                else{
                    $('.hidableOptions').css("visibility", "visible"); // Show the inputs
                    $('#taskFollowUpAction, #taskFollowUpHolder, #taskFollowUpDate').prop('required', true); // Enable the inputs and add required attribute
                }
            })            

});