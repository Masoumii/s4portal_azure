<?php

/* Main class */
class Notification
{

    /* Notification variables */
    private $nId            = null;
    private $nTitle         = null;
    private $nContent       = null;
    private $nIconId        = null;
    private $nIconName      = null;
    private $nIconSrc       = null;
    private $nReceiverId    = null;
    private $nReceiverName  = null;
    private $nSenderId      = null;
    private $nSenderName    = null;
    private $nCount         = null;
    private $userId         = null;
    private $userRol        = null;
    private $nCreatedOn     = null;

    /* Constructor */
    public function __construct(){}

    /* Create notification */
    public function createNotification($title, $content, $iconID, $senderID, $receiverID)
    {
        $this->nTitle      = $title;
        $this->nContent    = $content;
        $this->nIconId     = $iconID;
        $this->nSenderId   = $senderID;
        $this->nReceiverId = $receiverID;
        
        require_once "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "INSERT INTO `notification_system_messages` (`title`, `content`, `icon`, `sender`, `receiver`) VALUES ('$this->nTitle', '$this->nContent', '$this->nIconId', '$this->nSenderId', '$this->nReceiverId')";
        $stmt = $conn->prepare($q);
        // Query executed
        if($stmt->execute()){
            //echo "Notification created!";
        }else{
            //echo "Notification could not be created";
        }

    }

    /* Get all notifications */
    public function getAllNotifications($userId, $userRol)
    {
        $this->userId = $userId;
        $this->userRol = $userRol;

        require_once "../dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT 
        notification_system_messages.id AS `nId`, 
        notification_system_messages.title AS `nTitle`,
        notification_system_messages.content AS `nContent`,
        notification_system_messages.icon AS `nIconId`,
        notification_system_icons.name AS `nIconName`,
        notification_system_icons.src AS `nIconSrc`,
        notification_system_messages.receiver AS `nReceiverId`,
        notification_system_messages.sender AS `nSenderId`,
        receiver.gebruikersnaam AS `nReceiverName`,
        sender.gebruikersnaam AS `nSenderName`,
        notification_system_messages.created_on AS `nCreatedOn`
        FROM notification_system_messages
        INNER JOIN notification_system_icons ON notification_system_icons.id = notification_system_messages.icon
        INNER JOIN gebruikers AS `receiver` ON receiver.id = notification_system_messages.receiver
        INNER JOIN gebruikers AS `sender` ON sender.id = notification_system_messages.sender
        WHERE notification_system_messages.receiver = '$this->userId'
        ORDER BY notification_system_messages.created_on DESC";

        $stmt = $conn->prepare($q);
            // Query executed
        if($stmt->execute())
        {
            while ($row = $stmt->fetch())
            {
               $this->nId           = $row['nId'];
               $this->nTitle        = $row['nTitle'];
               $this->nContent      = $row['nContent'];
               $this->nIconId       = $row['nIconId'];
               $this->nIconName     = $row['nIconName'];
               $this->nIconSrc      = $row['nIconSrc'];
               $this->nReceiverId   = $row['nReceiverId'];
               $this->nReceiverName = $row['nReceiverName'];
               $this->nSenderId     = $row['nSenderId'];
               $this->nSenderName   = $row['nSenderName'];
               $this->nCreatedOn    = date('d-m-Y H:i',strtotime($row['nCreatedOn']));
               ?>
                <a href="#" class="nitem notify-item">
                        <div class="notify-text">
                            <p>
                                <img class="n-icon" src="<?=$this->nIconSrc?>" alt="<?=$this->nIconName?>" title="<?=$this->nIconName?>">
                                <span class="n-title"><?=$this->nTitle?></span>
                            </p>
                            <span class="msg"><?=$this->nContent?></span>
                            <span class="n-sender"><?=$this->nSenderName?></span>&nbsp;
                            <span class="n-created-on"><?=$this->nCreatedOn?></span>
                        </div>
                    </a>
            <?php
            }
        }
        else{
            // Query not executed
            echo "Query not executed";
        }
    }

    /* Count all notifications */
    public function countAllNotifications($userId)
    {
        $this->userId = $userId;
        require_once "../dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT * FROM notification_system_messages WHERE receiver='$this->userId'";
        $stmt = $conn->prepare($q);
        
        if($stmt->execute()){
            $this->nCount = $stmt->rowCount();
            echo $this->nCount;
        }else{
            // Query not executed
        }
    }

    /* Delete notification by ID */
    public function deleteNotificationById($nId)
    {
        $this->nId = $nId;
        require_once "../dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "DELETE FROM notification_system_messages WHERE receiver='$this->nId'";
        $stmt = $conn->prepare($q);
        if($stmt->execute()){
            echo "SUCCESS: Query (delete notification by ID) executed";
        }else{
            echo "ERROR: Query (delete notification by ID) NOT executed!";
        }
    }

        /* Mark all notifications as read */
        public function markAsRead($userId)
        {
            $this->userId = $userId;
            require_once "../dbconfig.php";
            $conn = DatabaseConnection::getConnection();
            $q = "DELETE FROM notification_system_messages WHERE receiver='$this->userId'";
            $stmt = $conn->prepare($q);
            if($stmt->execute()){
                echo "SUCCESS: Query (Mark as read) executed";
            }else{
                echo "ERROR: Query (Mark as read) NOT executed!";
            }
        }

}
?>