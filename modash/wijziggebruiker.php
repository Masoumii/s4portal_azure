<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}

/* If user is not logged in, redirect him back to login page */
if ($_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

/* Initialise the class and assign variable to use it below */
require "classes/s4portal.class.php";
$S4Portal = new S4Portal();

/* If user is not an S4Portal Administrator, redirect user back to index / hide this page */
$S4Portal->restrictAccess();
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>S4Portal</title>
    <?php require "headers.php"; ?>
</head>

<body>

    <!-- preloader area start -->
    <?php $S4Portal->renderPreloader(); ?>
    <!-- preloader area end -->

    <!-- page container area start -->
    <div class="page-container">

        <!-- Render the menu -->
        <?php $S4Portal->renderMenu(); ?>

        <!-- main content area start -->
        <div class="main-content">

            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">

                    <!-- nav and search button start -->
                    <?php $S4Portal->renderNavAndSearch(); ?>
                    <!-- nav and search button end -->

                    <!-- profile info & task notification start -->
                    <?php $S4Portal->renderProfileAndNotifications(); ?>
                    <!-- profile info & task notification end -->

                </div>

            </div>

            <!-- header area end -->

            <!-- page title area start -->
            <?php $S4Portal->renderPageTitle("Beheer"); ?>
            <!-- page title area end -->

            <div class="main-content-inner">

                <!-- row area start -->
                <div class="row-fluid">
                    <br>
                    <!-- Accordeon menu -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <?=$S4Portal->editUserDetails($_GET['editUser']);?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row area end -->

                <div class="row mt-5"></div>

            </div>
        </div>
        <!-- main content area end -->

        <!-- footer area start -->
        <?php $S4Portal->renderFooter(); ?>
        <!-- footer area end -->
    </div>

    <!-- Headers bottom -->
    <?php require "headersBottom.php"; ?>
</body>

</html>