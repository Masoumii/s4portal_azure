<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}

/* If user is not logged in, redirect him back to login page */
if ($_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

/* Initialise the class and assign variable to use it below */
require "classes/s4portal.class.php";
$S4Portal = new S4Portal();
?>

<!doctype html>
<!-- no-js until js is attached to it -->
<div class="no-js" lang="nl">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>S4Portal</title>

        <!-- Top headers -->
        <?php require "headers.php"; ?>
    </head>

    <body>
        <!-- preloader area start -->
        <div id="preloader">
            <div class="loader"></div>
        </div>
        <!-- preloader area end -->

        <!-- page container area start -->
        <div class="page-container">

            <!-- sidebar menu area start -->
            <?php $S4Portal->renderMenu(); ?>
            <!-- sidebar menu area end -->

            <!-- main content area start -->
            <div class="main-content">

                <!-- header area start -->
                <div class="header-area">
                    <div class="row align-items-center">

                        <!-- nav and search button start -->
                        <?php $S4Portal->renderNavAndSearch(); ?>
                        <!-- nav and search button end -->

                        <!-- profile info & task notification start -->
                        <?php $S4Portal->renderProfileAndNotifications(); ?>
                        <!-- profile info & task notification end -->

                    </div>
                </div>
                <!-- header area end -->

                <!-- page title area start -->
                <?php $S4Portal->renderPageTitle("Contactpersonen"); ?>
                <!-- page title area end -->

                <div class="main-content-inner">

                    <!-- Modal -->
                    <form id="editContactDetails" action="" method="POST">

                        <div class="modal fade" id="contactModal" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title noselect"><i class="ti-pencil-alt"></i>&nbsp;&nbsp;Contactpersoongegevens wijzigen</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                                    </div>

                                    <div class="modal-body">

                                        <!-- Contact ID -->
                                        <input name="conID" id="editConID" class="editConID" type="hidden">
                                        <br>

                                        <!-- Contact Organisation -->
                                        <label for="editConOrg">Organisatie</label>
                                        <select name="conOrg" id="editConOrg" class="editConOrg form-control col-sm-12" required>
                                            <?=$S4Portal->getAllOrganisationOptions()?>
                                        </select>
                                        <br>

                                         <!-- Contact type/role -->
                                         <label for="editConType">Type contactpersoon</label>
                                        <select name="conType" id="editConType" class="editConType form-control col-sm-12" required>
                                            <?=$S4Portal->getAllRoleOptions()?>
                                        </select>
                                        <br>


                                        <!-- Contact Sex --> 
                                        <label for="editConSex">Geslacht</label>
                                        <select name="conSex" id="editConSex" class="editConSex form-control col-sm-12" required>
                                            <option value="man">Man</option>
                                            <option value="vrouw">Vrouw</option>
                                        </select>
                                        <br>

                                        <!-- Contact Firstname -->
                                        <label for="editConFirstName">Voornaam</label>
                                        <input name="conFirstName" id="editConFirstName" class="editConFirstName form-control col-sm-12" type="text" required>
                                        <br> 

                                        <!-- Contact Lastname -->
                                        <label for="editConLastName">Achternaam</label>
                                        <input name="conLastName" id="editConLastName" class="editConLastName form-control col-sm-12" type="text" required>
                                        <br> 

                                        <!-- Contact function -->  
                                        <label for="editConFunction">Functie</label>
                                        <input name="conFunction" id="editConFunction" class="editConFunction form-control col-sm-12" type="text" required>
                                        <br>

                                        <!-- Contact email -->
                                        <label for="editConEmail">Emailadres</label>
                                        <input name="conEmail" id="editConEmail" class="editConEmail form-control col-sm-12" type="text" required>
                                        <br> 

                                        <!-- Contact phone: landline --> 
                                        <label for="editConLandline">Telefoon vast</label>
                                        <input name="conLandline" id="editConLandline" class="editConLandline form-control col-sm-12" type="text">
                                        <br>

                                        <!-- Contact phone: mobile --> 
                                        <label for="editConMobile">Telefoon mobiel</label>
                                        <input name="conMobile" id="editConMobile" class="editConMobile form-control col-sm-12" type="text">
                                        <br> 
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                        <button type="submit" name="saveOrgChanges" class="btn btn-primary">Wijziging opslaan</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row-fluid">
                        <br>
                        <!-- Accordeon menu -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title noselect" style="color:#08475b"><i class="ti-contacts"></i>&nbsp;&nbsp;Contactpersonen aanmaken</h4>
                                    <!-- Start accordion menu -->
                                    <div id="accordion2" class="according accordion-s2">
                                        <!-- TAB 1 -->
                                        <?= $S4Portal->renderNewContactSalesTab(); ?>
                                        <!-- TAB 2 -->
                                        <?= $S4Portal->renderNewContactSupportTab(); ?>
                                        <!-- TAB 3 -->
                                        <?= $S4Portal->renderNewContactAdminTab(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- row area start -->
                    <div class="row-fluid">
                        <div class="col-lg-12">

                            <br>
                            <!-- Show error message if no organisations found -->
                            <?php if (!$S4Portal->orgExistence) {
                                echo "Maak eerst een organisatie aan";
                            }; ?>
                        </div>

                        <div class="col-md-12 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title noselect" style="color:#08475b"><i class="ti-contacts"></i>&nbsp;Alle contactpersonen</h4>
                                    <div class="single-table">
                                        <div class="table-responsive">
                                            <table id="contacts" class="table table-hover progress-table text-center">
                                                <thead class="text-uppercase">
                                                    <tr>
                                                        <th scope="col">Org.</th>
                                                        <th scope="col">Rol</th>
                                                        <th scope="col">Voornaam</th>
                                                        <th scope="col">Achternaam</th>
                                                        <th scope="col">Functie</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Tel:vast</th>
                                                        <th scope="col">Tel:mobiel</th>
                                                        <th scope="col">Type</th>
                                                        <th scope="col">Acties</th>
                                                    </tr>
                                                </thead>
                                                <!-- Table body -->
                                                <tbody>
                                                    <?= $S4Portal->renderContactpersonenTable(); ?>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- main content area end -->

            <!-- footer area start-->
            <?php $S4Portal->renderFooter(); ?>
            <!-- footer area end-->
        </div>

        <!-- Bottom headers required on all pages -->
        <?php require "headersBottom.php"; ?>
</div>
</body>