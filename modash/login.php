<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if(!isset($_SESSION)){session_start();}

/* If login form submitted & password has value */
if(isset($_POST['login'])){

    $userEmail    = strip_tags(trim($_POST['email']));
    $userPassword = strip_tags(trim($_POST['password']));

    /* Attempt to log the user in with the user input */
    require_once('classes/login.class.php');
    $user = new Login();
    $user->logUserIn($userEmail, $userPassword);
}


    /* If user is logged in */
    if(isset($_SESSION['loggedIn'])){
         if($_SESSION['loggedIn'] === true){
            header("Location: index.php");
         }
    }

?>

<!doctype html>
<html class="no-js" lang="en">

<!-- The video -->
<video autoplay muted loop id="myVideo">
  <source src="assets/s4portal.mp4" type="video/mp4">
</video>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login - S4Portal</title>
    <?php require "headers.php"; ?>
</head>

<body>

    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->

    <!-- login area start -->
    <div class="login-area login-bg">
        <div class="container">
            <div class="login-box ptb--100">
                <form method="POST" action="">
                    <div class="login-form-head">
                        <!-- <h4>Log in</h4>
                        <p>Log in op S4Portal</p> -->
                    </div>
                    <div class="login-form-body">
                        <div class="form-gp">
                            <label for="exampleInputEmail1">Emailadres</label>
                            <input name="email" style="text-align: center" type="email" id="exampleInputEmail1">
                            <i class="ti-email"></i>
                        </div>
                        <div class="form-gp">
                            <label for="exampleInputPassword1">Wachtwoord</label>
                            <input name="password" style="text-align: center" type="password" id="exampleInputPassword1">
                            <i class="ti-lock"></i>
                        </div>

                        <div class="submit-btn-area">
                            <button name="login" id="login" type="submit">Inloggen <i class="ti-arrow-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- login area end -->

    <!-- jquery latest version -->
    <script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/jquery.slicknav.min.js"></script>
    
    <!-- others plugins -->
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>
</html>