<?php
/* Show all errors : Development */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/* Start session if not already started */
if(!isset($_SESSION)){session_start();}

/* Log user out */
if(isset($_GET['logout'])){
    require_once "classes/login.class.php";
    $loginClass = new Login();
    $loginClass->logUserOut();
    }

    /* Add new contact ( Organisation contact ) */
if(isset($_GET['newContact'])){
    $orgId        = $_POST['organisation'];
    $conSex       = $_POST['sex'];
    $conFirstname = $_POST['firstname'];
    $conLastname  = $_POST['lastname'];
    $conFunction  = $_POST['function'];
    $conEmail     = $_POST['email'];
    $conLandline  = $_POST['landline'];
    $conMobile    = $_POST['mobile'];
    $conType      = $_POST['conType'];
    $conMain      = $_POST['main'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->createNewContact($orgId, $conSex, $conFirstname, $conLastname, $conFunction, $conEmail, $conLandline, $conMobile, $conType, $conMain);
}

/* Edit contact ( organisation contact ) */
if(isset($_GET['editContact'])){
    $conId        = $_POST['conID'];
    $conOrg       = $_POST['conOrg'];
    $conType      = $_POST['conType'];
    $conSex       = $_POST['conSex'];
    $conFirstName = $_POST['conFirstName'];
    $conLastName  = $_POST['conLastName'];
    $conFunction  = $_POST['conFunction'];
    $conEmail     = $_POST['conEmail'];
    $conLandLine  = $_POST['conLandline'];
    $conMobile    = $_POST['conMobile'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->editContact($conId, $conOrg, $conType, $conSex, $conFirstName, $conLastName, $conFunction, $conEmail, $conLandLine, $conMobile);
}

/* Follow up a task */
if(isset($_POST['followUpTask']))
{

    if($_POST['originalTaskId']==="" || empty($_POST['originalTaskId']) || !isset($_POST['originalTaskId'])){

        $originalTaskId        = $_POST['taskId']; 
    }else{

        $originalTaskId = $_POST['originalTaskId'];
    }

    $taskContent           = $_POST['taskContent'];           // Task(description/content)
    $taskOrgId             = $_POST['taskOrgId'];             // The original ID of the task
    $taskRolId             = $_POST['taskRolId'];             // Task Rol (sales, support etc)
    $taskFollowupActionID  = $_POST['taskFollowupActionId'];  // Task followup action ID
    $taskFollowupDate      = $_POST['taskFollowupDate'];      // Task followup date
    $taskFollowupHolderID  = $_POST['taskFollowupHolderId'];  // Task followup holder ID
    $taskFollowupCreatorID = $_POST['taskCreatorId'];         // Task followup creator ID

    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->followUpTask($originalTaskId, $taskContent, $taskOrgId, $taskRolId,  $taskFollowupActionID, $taskFollowupHolderID, $taskFollowupCreatorID, $taskFollowupDate);
}

/* Add new task */
if(isset($_POST['createTask'])){

    /* Task FollowUp Boolean */
    $taskFollowUpBoolean = $_POST['taskFollowUp'];

    /* If there is a follow up */
        $task                = $_POST['task'];
        $taskRol             = $_POST['taskRol'];
        $taskOrg             = $_POST['taskOrg'];

        if(isset($_POST['taskFollowUpAction'])){
            $taskFollowUpAction  = $_POST['taskFollowUpAction'];
            $taskFollowUpHolder  = $_POST['taskFollowUpHolder'];
        }else{
            $taskFollowUpAction  = 'NULL';
            $taskFollowUpHolder  = 'NULL';
        }
       
        $taskFollowUpDate    = $_POST['taskFollowUpDate'];
        $taskCreator         = $_SESSION['user_id']; 
        require_once "classes/s4portal.class.php";
        $S4Portal = new S4Portal();
        $S4Portal->createNewTask($task, $taskRol, $taskFollowUpAction, $taskFollowUpHolder, $taskFollowUpDate, $taskCreator, $taskOrg, $taskFollowUpBoolean);
    
}

/* Add new user ( S4Portal user ) */
if(isset($_GET['newUser'])){
    $username     = $_POST['username'];
    $password     = $_POST['password'];
    $rol          = $_POST['rol'];
    $email        = $_POST['email'];
    $isAdmin      = $_POST['isAdmin'];
    $isRelManager = $_POST['isRelManager'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->createUser($username, $password, $rol, $email, $isAdmin, $isRelManager);
};

/* Add new organisation */
if(isset($_GET['newOrganisation'])){
    $orgId             = null;
    $orgNumber         = $_POST['orgNumber'];
    $orgName           = $_POST['orgName'];
    $orgAddress        = $_POST['orgAddress'];
    $orgHouseNumber    = $_POST['orgHouseNumber'];
    $orgPostalCode     = $_POST['orgPostalCode'];
    $orgCity           = $_POST['orgCity'];
    $orgCountry        = $_POST['orgCountry'];
    $orgPhone          = $_POST['orgPhone'];
    $orgEmail          = $_POST['orgEmail'];
    $orgStatus         = $_POST['orgStatus'];
    $orgNotes          = $_POST['orgNotes'];
    $orgERP            = $_POST['orgERP'];
    $orgCMS            = $_POST['orgCMS'];
    $orgIns            = $_POST['orgINS'];
    $orgInfDesk        = $_POST['orgInfDesk'];
    $orgIncassoPartner = $_POST['orgIncassoPartner'];
    $orgS4Prod         = $_POST['orgS4Prod'];
    $orgRel            = $_POST['orgRel'];
    $userID            = $_SESSION['user_id'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->createNewOrganisation(
        $orgId,
        $orgNumber,
        $orgName,
        $orgAddress,
        $orgHouseNumber,
        $orgPostalCode,
        $orgCity,
        $orgCountry,
        $orgPhone,
        $orgEmail,
        $orgStatus,
        $orgNotes,
        $orgERP,
        $orgCMS,
        $orgIns,
        $orgInfDesk,
        $orgIncassoPartner,
        $orgS4Prod,
        $orgRel,
        $userID
    );
}

/* Delete file */
if(isset($_GET['del_id']) && isset($_GET['del_name'])){

    $fileId = $_GET['del_id'];
    $fileName = $_GET['del_name'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->deleteFile($fileId, $fileName);
}

/* Edit S4Portal user */
if(isset($_GET['editUser'])){
    /* Get POST variables from post */
    $userid           = $_POST['userId'];
    $username         = $_POST['userName'];
    $useremail        = $_POST['userEmail'];
    $userpass         = $_POST['userPass'];
    $userrol          = $_POST['userRol'];
    $userisadmin      = $_POST['isAdmin'];
    $userisrelmanager = $_POST['isRelManager'];
    $userisactive     = $_POST['isActive'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->editUser($userid, $username, $useremail, $userpass, $userrol, $userisadmin, $userisrelmanager, $userisactive);
}


/* Delete S4Portal user */
if(isset($_GET['deleteUser'])){
    $userId = $_GET['deleteUser'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->deleteUser($userId);
}


/* Edit organisation details */
if(isset($_GET['editOrg'])){

    /* Get POST variables from post */
    $orgid      = $_POST['orgId'];
    $orgnumber  = $_POST['orgNum'];
    $orgname    = $_POST['orgName'];
    $orgaddress = $_POST['orgAddress'];
    $orghousenr = $_POST['orgHousenr'];
    $orgpostal  = $_POST['orgPostal'];
    $orgcity    = $_POST['orgCity'];
    $orgcountry = $_POST['orgCountry'];
    $orgtel     = $_POST['orgPhone'];
    $orgemail   = $_POST['orgEmail'];
    $orgstatus  = $_POST['orgStatus'];
    $orgerp     = $_POST['orgERP'];
    $orgcms     = $_POST['orgCMS'];
    $orgins     = $_POST['orgIns'];
    $orginf     = $_POST['orgInf'];
    $orgincasso = $_POST['orgIncasso'];
    $orgs4prod  = $_POST['orgS4Prod'];
    $orgrel     = $_POST['orgRel'];
    $orgnotes   = $_POST['orgNotes'];

    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->editOrganisation($orgid,$orgnumber,$orgname,$orgaddress,$orghousenr,$orgpostal,$orgcity,$orgcountry,$orgtel,$orgemail,$orgstatus,$orgerp,$orgcms,$orgins,$orginf,$orgincasso,$orgs4prod,$orgrel,$orgnotes);
}

/* Delete organisation */
if(isset($_GET['deleteOrg'])){
    $orgId = $_GET['deleteOrg'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->deleteOrganisation($orgId);
}

/* Autocomplete Search: Organisations */
if(isset($_POST['keyword'])){
    $keyword = $_POST['keyword'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->searchOrganisation($keyword);
}

/* Delete contact */
if(isset($_GET['deleteContact'])){
    $conId = $_GET['conId'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
}

/* Get task history */
if(isset($_POST['historyId'])){
    
    $historyId = $_POST['historyId'];
    require_once "classes/s4portal.class.php";
    $S4Portal = new S4Portal();
    $S4Portal->renderTaskHistoryModal($historyId);
}
?>