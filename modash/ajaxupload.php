<?php

   if(isset($_FILES['file'])){

      $errors= array();
      $file_name = $_FILES['file']['name'];
      $file_size =$_FILES['file']['size'];
      $file_tmp =$_FILES['file']['tmp_name'];
      $file_type=$_FILES['file']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));
      
      $extensions= array("pdf","doc","docx","xls","xlxs","txt");
      
      if(in_array($file_ext,$extensions)=== false){
         $errors[]="Gekozen bestandstype is niet toegestaan.";
      }
      
      if($file_size > 10097152){
         $errors[]='Bestand is te groot. ( max 10 mb )';
      }

      if(empty($errors)==true){

        $uploadDir = "upload/";
         move_uploaded_file($file_tmp,$uploadDir.$file_name);
         echo $file_name." is succesvol geupload naar ".$uploadDir;

      }else{
         print_r($errors);
      }
   }
?>
<html>
   <body>
      
      <form action="" method="POST" enctype="multipart/form-data">
         <input type="file" name="file" />
         <input type="submit"/>
      </form>
      
   </body>
</html>