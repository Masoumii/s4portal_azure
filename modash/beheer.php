<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}

/* If user is not logged in, redirect him back to login page */
if ($_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

/* Initialise the class and assign variable to use it below */
require "classes/s4portal.class.php";
$S4Portal = new S4Portal();

/* If user is not an S4Portal Administrator, redirect user back to index / hide this page */
$S4Portal->restrictAccess();

?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>S4Portal</title>
    <?php require "headers.php"; ?>
</head>

<body>

    <!-- preloader area start -->
    <?php $S4Portal->renderPreloader(); ?>
    <!-- preloader area end -->

    <!-- page container area start -->
    <div class="page-container">

        <!-- Render the menu -->
        <?php $S4Portal->renderMenu(); ?>

        <!-- main content area start -->
        <div class="main-content">

            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">

                    <!-- nav and search button start -->
                    <?php $S4Portal->renderNavAndSearch(); ?>
                    <!-- nav and search button end -->

                    <!-- profile info & task notification start -->
                    <?php $S4Portal->renderProfileAndNotifications(); ?>
                    <!-- profile info & task notification end -->

                </div>

            </div>

            <!-- header area end -->

            <!-- page title area start -->
            <?php $S4Portal->renderPageTitle("Beheer"); ?>
            <!-- page title area end -->

            <div class="main-content-inner">

                <!-- row area start -->
                <div class="row-fluid">
                    <br>
                    
                    <!-- Accordeon menu -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- Page title -->
                                <h4 class="header-title noselect" style="color:#08475b"><i class="ti-contacts"></i>&nbsp;&nbsp;Gebruikersbeheer</h4>
                                <br>
                                <!-- Start accordion menu -->
                                <div id="accordion2" class="according accordion-s2">
                                    <!-- TAB 1: 'Gebruiker aanmaken' tab inside Accordion menu -->
                                    <?= $S4Portal->renderUserCreationTab(); ?>
                                    <!-- TAB 2: 'Gebruiker wijzigen/verwijderen' tab inside accordion menu -->
                                    <?= $S4Portal->renderUserEditAndDeleteTab(); ?>
                                    <!-- TAB 3 'Taken acties aanmaken' -->
                                    <!-- End accordion menu -->
                                </div>
                            </div>
                        </div>

                        <!-- Gebruikers Modal -->
                        <form id="editUserDetails" action="" method="POST">
                        <div class="modal fade" id="exampleModalLong" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title noselect"><i class="ti-pencil-alt"></i>&nbsp;&nbsp;Gebruikersgegevens wijzigen</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                                    </div>
                                    <div class="modal-body">

                                        <!-- User ID (Hidden)-->
                                        <input type="hidden" name="userId" id="editUserId" class="editUserId">

                                        <!-- Username -->
                                        <label for="editUsername">Naam</label>
                                        <input name="userName" id="editUsername" class="editUsername form-control col-sm-12" type="text">
                                        <br>

                                        <!-- Email -->
                                        <label for="editEmail">Emailadres</label>
                                        <input name="userEmail" id="editEmail" type="text" class="editEmail form-control col-sm-12">
                                        <br>

                                        <!-- Password -->
                                        <label for="editPassword">Wachtwoord</label>
                                        <input name="userPass" id="editPassword" type="password" class="editPassword form-control col-sm-12">
                                        <br>

                                        <!-- Rol -->
                                        <label for="rol" class="col-form-label noselect">* Rol</label>
                                        <select name="userRol" class="editRol form-control" id="rol" name="rol" required>
                                            <?=$S4Portal->getAllRoleOptions()?>
                                        </select>
                                        <br>

                                        <!-- IsAdmin -->
                                        <label for="isAdmin" class="col-form-label noselect">* S4Portal Administrator</label>
                                        <select class="editIsAdmin form-control" id="isAdmin" name="isAdmin" required>
                                            <option value="1">Ja</option>
                                            <option value="0">Nee</option>
                                        </select>
                                        <br>

                                        <!-- isRelatieBeheerder -->
                                        <label for="isRelManager" class="col-form-label noselect">* Relatiebeheerder</label>
                                        <select class="editIsRelManager form-control" id="isRelManager" name="isRelManager" required>
                                            <option value="1">Ja</option>
                                            <option value="0">Nee</option>
                                        </select>
                                        <br>

                                        <!-- isActive -->
                                        <label for="isActive" class="col-form-label noselect">* Actief</label>
                                        <select class="editIsActive form-control" id="isActive" name="isActive" required>
                                            <option value="1">Ja</option>
                                            <option value="0">Nee</option>
                                        </select>
                                        <br>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                        <button type="submit" name="saveChanges" class="btn btn-primary">Wijziging opslaan</button>
                                    

                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>
                <!-- row area end -->

                <div class="row mt-5"></div>
            </div>
        </div>
        <!-- main content area end -->

        <!-- footer area start -->
        <?php $S4Portal->renderFooter(); ?>
        <!-- footer area end -->
    </div>

    <!-- Headers bottom -->
    <?php require "headersBottom.php"; ?>
</body>

</html>